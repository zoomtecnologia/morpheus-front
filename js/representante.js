let representante = {};

let representanteServico = {
    salvar: async function (representante) {
        delete representante.status;
        return requisicao.requisitarMetodoPOST("/representantes", representante, false);
    },
    buscarRepresentante: async function (documento) {
        return requisicao.requisitarMetodoGET(`/representantes/${documento}`);
    },
    alterar: async function (representante) {
        const urlAlteracao = `/representantes/${representante.documento}`;
        delete representante.documento;
        return requisicao.requisitarMetodoPUT(urlAlteracao, representante, false, false);
    }
};

let representanteControle = {
    alteracao: false,
    salvar: async function () {
        try {
            const representante = this.preencherRepresentante();
            let resposta;
            if (this.alteracao) {
                resposta = await (await representanteServico.alterar(representante)).text();
            } else {
                resposta = await (await representanteServico.salvar(representante)).json();
                this.limparCampos();
            }
            if (resposta.mensagemDesenvolvedor) {
                swal("Morpheus", resposta.mensagemDesenvolvedor, "warning");
            } else {
                swal("Morpheus", "Salvo com sucesso!!", "success");
            }
        } catch (exception) {
            const mensagem = !exception.mensagemDesenvolvedor ? "Não foi possivel se conectar ao servidor" : exception.mensagemDesenvolvedor;
            swal("Morpheus", mensagem, "error");
        }
    },
    preencherRepresentante: function () {
        representante.documento = document.getElementById("documento").value;
        representante.nome = document.getElementById("nome").value;
        representante.nomeSocial = document.getElementById("nomeSocial").value;
        representante.logradouro = document.getElementById("logradouro").value;
        representante.numero = document.getElementById("numero").value;
        representante.complemento = document.getElementById("complemento").value;
        representante.bairro = document.getElementById("bairro").value;
        representante.cidade = document.getElementById("cidade").value;
        representante.estado = document.getElementById("estado").value;
        representante.cep = document.getElementById("cep").value.replace(/\D/g, "");
        representante.nomeContato = document.getElementById("nomeContato").value;
        representante.telefone = document.getElementById("telefone").value.replace(/\D/g, "");;
        representante.celular = document.getElementById("celular").value.replace(/\D/g, "");;
        representante.whatsapp = document.getElementById("whatsapp").value.replace(/\D/g, "");;
        representante.email = document.getElementById("email").value;
        representante.tipo = document.getElementById("suporte").checked ? "S" : "C";
        representante.diaPagamentoComissao = document.getElementById("diaPagamentoComissao").value;
        representante.diasToleranciaPagamentoComissao = document.getElementById("diasToleranciaPagamentoComissao").value;
        representante.usuarioAtualizacao = usuario.nome;//;
        representante.nivel = document.getElementById("nivel").value;
        representante.obs = document.getElementById("obs").value;
        representante.banco = document.getElementById("banco").value;
        representante.agencia = document.getElementById("agencia").value;
        representante.conta = document.getElementById("conta").value;
        representante.tipoConta = document.getElementById("tipoConta").value;
        representante.documentoPessoaConta = document.getElementById("documentoPessoaConta").value;
        representante.nomePessoaConta = document.getElementById("nomePessoaConta").value;
        representante.chavePix = document.getElementById("chavePix").value;
        representante.status = document.getElementById("statusAtividade").checked ? "I" : "A";
        representante.geraReceita = document.getElementById("geraReceita").checked;
        return representante;
    },
    async buscarRepresentante(cnpj) {
        const promise = await representanteServico.buscarRepresentante(cnpj);
        const json = await promise.json();
        document.getElementById("documento").value = json.documento;
        document.getElementById("nome").value = json.nome;
        document.getElementById("nomeSocial").value = json.nomeSocial;
        document.getElementById("logradouro").value = json.logradouro;
        document.getElementById("numero").value = json.numero;
        document.getElementById("complemento").value = json.complemento;
        document.getElementById("bairro").value = json.bairro;
        document.getElementById("cidade").value = json.cidade;
        document.getElementById("estado").value = json.estado;
        document.getElementById("cep").value = json.cep;
        document.getElementById("nomeContato").value = json.nomeContato;
        document.getElementById("telefone").value = json.telefone;
        document.getElementById("celular").value = json.celular;
        document.getElementById("whatsapp").value = json.whatsapp;
        document.getElementById("email").value = json.email;
        document.getElementById("geraReceita").checked = json.geraReceita;
        document.getElementById("suporte").removeAttribute("checked");
        if (json.tipo == "S") {
            document.getElementById("suporte").setAttribute("checked", "checked");
        }
        if (json.status == "I") {
            document.getElementById("statusAtividade").setAttribute("checked", "checked");
        }
        document.getElementById("diaPagamentoComissao").value = json.diaPagamentoComissao;
        document.getElementById("diasToleranciaPagamentoComissao").value = json.diasToleranciaPagamentoComissao;
        document.getElementById("nivel").value = json.nivel;
        document.getElementById("obs").value = json.obs;
        document.getElementById("banco").value = json.banco;
        document.getElementById("agencia").value = json.agencia;
        document.getElementById("conta").value = json.conta;
        document.getElementById("tipoConta").value = json.tipoConta;
        document.getElementById("documentoPessoaConta").value = json.documentoPessoaConta;
        document.getElementById("nomePessoaConta").value = json.nomePessoaConta;
        document.getElementById("chavePix").value = json.chavePix;
    },
    desabilitarBotaoGravar: function () {
        document.getElementById("btGravarRepresentante").setAttribute("disabled", "disabled");
        document.getElementById("loading").classList.remove("d-none");
    },
    habilitarBotaoGravar: function () {
        document.getElementById("btGravarRepresentante").removeAttribute("disabled");
        document.getElementById("loading").classList.add("d-none");
    },
    limparCampos: function () {
        const inputs = document.querySelectorAll("input");
        inputs.forEach(elemento => {
            elemento.value = "";
        });
        const checks = document.querySelectorAll("input[type='checkbox']");
        checks.forEach(elemento => {
            elemento.removeAttribute("checked");
        });
    }
};

let formularioRepresentante = document.getElementById("formularioRepresentante");
formularioRepresentante.addEventListener("submit", event => {
    event.preventDefault();
    representanteControle.desabilitarBotaoGravar();
    representanteControle.salvar().then(() => {
        representanteControle.habilitarBotaoGravar();
    });
});
let campoCep = document.getElementById("cep");
campoCep.addEventListener("change", async event => {
    const promisse = await viaCep.buscarCep(campoCep.value);
    const resposta = await promisse.json();
    document.getElementById("logradouro").value = resposta.logradouro;
    document.getElementById("bairro").value = resposta.bairro;
    document.getElementById("cidade").value = resposta.localidade;
    document.getElementById("estado").value = resposta.uf;
    document.getElementById("numero").focus();
});

window.addEventListener("load", () => {
    $('.celular').mask('(00)00000-0000');
    $('.cep').mask('00000-000');
    const parametro = window.location.search.slice(1);
    const representanteId = parametro.replace(/documento=/, "");
    if (!representanteId) {
        representanteControle.alteracao = false;
        document.getElementById("geraReceita").checked = true;
        return;
    }
    const conteudo = document.getElementById("conteudo");
    const spinner = document.getElementById("spinner");
    conteudo.classList.add("d-none");
    spinner.classList.remove("d-none");
    representanteControle.buscarRepresentante(representanteId).then(response => {
        representanteControle.alteracao = true;
        conteudo.classList.remove("d-none");
        spinner.classList.add("d-none");
    });
});