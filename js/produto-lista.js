const produtoService = {
    async listarItens() {
        return requisicao.requisitarMetodoGET("/itens");
    }
};

const produtoControle = {
    async listarItens() {
        const loading = document.getElementById("loading");
        const listaProdutos = document.getElementById("listaProdutos");
        listaProdutos.innerHTML = "";
        loading.classList.remove("d-none");
        let resposta = [];
        try {
            resposta = await (await produtoService.listarItens()).json();   
        } catch (error) {
            swal("Não foi possivel carregar as informações", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        loading.classList.add("d-none");
        let linhas = "";
        for (const item of resposta) {
            const badgeStatus = item.status == "I" ? "danger" : "success";
            const produtoUnico = item.itemUnico ? "Único" : "Por Quantidade";
            const produtoOpcional = item.opcional ? "Opcional" : "Obrigatorio";
            linhas += `
            <li class="list-group-item shadow mb-2 flex-column align-items-start">
            <div class="d-flex w-100 justify-content-start">
                <h6 class="mb-1 font-weight-bold" style="font-size: 14pt;">
                    ${item.descricao}
                </h6>
            </div>
            <h5 class="mt-1 font-weight-bold" style="text-align:right;">
               Valor R$ ${item.valor}
            </h5>
            <h5 class="mt-1 font-weight-bold" style="text-align:right;">
               <small>Valor minimo R$ ${item.valorMinimo}</small>
            </h5>
            <hr>
            <div class="d-flex w-100 justify-content-start">
                <span class="badge badge-${badgeStatus} mr-2">Ativo</span>
                <span class="badge badge-secondary mr-2">${produtoUnico}</span>
                <span class="badge badge-secondary">${produtoOpcional}</span>
            </div>
            <hr>
            <div class="d-flex w-100 justify-content-end">
                <button class="btn btn-grid-pg m-2" onclick="location.href='produtos.html?item=${item.codigo}'">
                    <span id="" class="spinner-grow spinner-grow-sm d-none"
                        role="status" aria-hidden="true"></span>
                    <i class="fas fa-edit"></i>
                </button>
            </div>
        </li>`;
        }
        listaProdutos.innerHTML = linhas;
    }
};