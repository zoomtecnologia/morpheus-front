const usuarioServico = {
    async buscarUsuarioPorToken(token) {
        return fetch(`${servidor.URL_API}/usuarios/${token}/recuperar-senha`);
    },
    async recuperarSenha(usuario) {
        return fetch(`${servidor.URL_API}/usuarios/recuperar-senha`, {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(usuario)
        });
    }
};

const usuarioControle = {
    usuario: {},
    usuarioEncontrado: {},
    async buscarUsuarioPorToken() {
        try {
            const resposta = await (await usuarioServico.buscarUsuarioPorToken(token)).json();
            if (resposta.mensagemDesenvolvedor) {
                location.href = "../../index.html";
                return;
            }
            this.usuarioEncontrado = resposta;
            document.querySelector("#email").textContent = this.usuarioEncontrado.email;
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
    },
    async recuperarSenha() {
        if (this.usuario.senha != this.usuario.confirmaSenha) {
            swal("Senhas não confere", "Morpheus", "warning");
            return;
        }
        this.usuario.email = this.usuarioEncontrado.email;
        const resposta = await (await usuarioServico.recuperarSenha(this.usuario)).json();
        if (resposta.mensagemDesenvolvedor) {
            swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
            return;
        }
        swal("Senha cadastrada", "Morpheus", "success").then(() => {
            localStorage.removeItem('usuario');
            location.href = "../../index.html";
        });
    }
};

const formulario = document.querySelector("#formulario");
const loginSenha = document.querySelector("#loginSenha");
const loginConfirmaSenha = document.querySelector("#loginConfirmaSenha");
formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    usuarioControle.usuario.senha = loginSenha.value;
    usuarioControle.usuario.confirmaSenha = loginConfirmaSenha.value;
    usuarioControle.recuperarSenha();
});

window.onload = function () {
    usuarioControle.buscarUsuarioPorToken();
};