const parametro = location.search.slice(1);
const numeroPedido = parametro.replace(/pedido=/, "");
if (!numeroPedido) {
    location.href = "https://mail.google.com/mail/u/0/?tab=rm&ogbl";
}
const contratoServico = {
    async buscarPedido(numeroPedido) {
        return requisicao.requisitarMetodoGET(`/pedidos/${numeroPedido}`);
    },
    async aceitarContrato(cliente) {
        return requisicao.requisitarMetodoPUT(`/clientes/${cliente}/aceite`, "", false, false);
    },
    async enviarEmailContratoAceito(cnpj) {
        return requisicao.requisitarMetodoPOST(`/clientes/${cnpj}/enviar-email-contrato-aceito`, "", true);
    }
};

const contratoControle = {
    pedido: {},
    async buscarPedido() {
        const pedido = await (await contratoServico.buscarPedido(numeroPedido)).json();
        return pedido;
    },
    async aceitarContrato() {
        const resposta = await (await contratoServico.aceitarContrato(this.pedido.cliente.cnpj)).text();
        contratoServico.enviarEmailContratoAceito(this.pedido.cliente.cnpj);
        window.location.reload();
    },
    visualizarContrato(pedido) {
        var options = {
            fallbackLink: `
                <div class="card" style="width: 100%">
                <div class="card-body">
                    <h5 class="card-title">Seu navegador não suporta a visualização do contrato</h5>
                    <p class="card-text">Aperte em baixar contrato </p>
                    <a class="btn btn-primary" href='[url]'>Baixar contrato</a>
                </div>
                </div>
                `
        };
        fetch(`${servidor.URL_API}/pedidos/${pedido.numero}/contrato`).then(promisse => {
            const resposta = promisse.arrayBuffer();
            resposta.then(dados => {
                const blob = new Blob([dados], { type: 'application/pdf' });
                const url = URL.createObjectURL(blob);
                PDFObject.embed(url, "#visualizador", options);
            });
        });
    }

};
window.onload = function () {
    const aceitaContrato = document.getElementById("aceitaContrato");
    const containerAceite = document.getElementById("container-aceite");
    contratoControle.buscarPedido().then(pedido => {
        contratoControle.pedido = pedido;
        contratoControle.visualizarContrato(pedido);
        const cliente = pedido.cliente;
        containerAceite.classList.remove("botao-aceite-disabled");
        if (cliente.aceiteContrato) {
            aceitaContrato.disabled = true;
            aceitaContrato.textContent = "Contrato Aceito " + cliente.dataAceite;
            containerAceite.classList.add("botao-aceite-disabled");
        }
    });
    aceitaContrato.addEventListener("click", evt => {
        contratoControle.aceitarContrato();
    });
};