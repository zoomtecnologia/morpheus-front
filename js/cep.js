var viaCep = {
    buscarCep: async function(cep){
        return fetch(`https://viacep.com.br/ws/${cep.replace(/\D/,"")}/json/`);
    }
}