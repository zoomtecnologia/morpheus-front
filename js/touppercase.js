const inputsText = document.querySelectorAll("input[type='text'],textarea");
inputsText.forEach(input => {
    input.addEventListener("keyup", evt => {
        input.value = input.value.replace("Ç", "C").toUpperCase();
    });
});