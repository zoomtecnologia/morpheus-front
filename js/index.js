const servidor = {
    URL_API: "https://morpheus-back.herokuapp.com/morpheus"
};
const usuario = JSON.parse(localStorage.getItem("usuario"));
if (usuario) {
    usuario.logout = function () {
        localStorage.removeItem('usuario');
        location.href = "../../index.html";
    };
}
const requisicao = {
    PUT: "PUT",
    POST: "POST",
    GET: "GET",
    URL_API: servidor.URL_API,
    requestInit: {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': usuario ? usuario.autorizacao : ""
        },
        body: {}
    },
    requisitarMetodoGET(url) {
        requisicao.requestInit.method = requisicao.GET;
        delete requisicao.requestInit.body;
        return fetch(`${this.URL_API}${url}`, this.requestInit);
    },
    requisitarMetodoPOST(url, conteudo, naoTemCorpo) {
        requisicao.requestInit.method = requisicao.POST;
        requisicao.requestInit.body = JSON.stringify(conteudo);
        if (naoTemCorpo) {
            delete requisicao.requestInit.body;
        }
        return fetch(`${this.URL_API}${url}`, requisicao.requestInit);
    },
    requisitarMetodoPUT(url, conteudo, naoTemCorpo, naoEJson) {
        requisicao.requestInit.method = requisicao.PUT;
        requisicao.requestInit.body = naoEJson ? conteudo : JSON.stringify(conteudo);
        if (naoTemCorpo) {
            delete requisicao.requestInit.body;
        }
        return fetch(`${this.URL_API}${url}`, requisicao.requestInit);
    }
};