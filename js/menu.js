(function(){
    const nomeUsuarioTela = document.getElementById("nomeUsuario");
    const palavras = usuario.nome.split(" ");
    let frase = "";
    for (const palavra of palavras) {
          frase += palavra[0].replace(palavra[0],palavra[0].toUpperCase())+palavra.toLowerCase().substring(1)+" ";  
    }
    nomeUsuarioTela.textContent = frase;
    const itensMenu = document.getElementById("itensMenu");
    const representante = usuario.permissoes.representante;
    if (!representante) {
        itensMenu.innerHTML = `
        <a class="nav-link" href="../importacao-arquivo/importacao-arquivo.html">
            <i class="fas fa-upload"></i>
            <span>Importação</span>
        </a>`;

    }
    itensMenu.innerHTML += `
        <a class="nav-link" href="../comissao/comissoes.html">
            <i class="fas fa-dollar-sign"></i>
            <span>Comissões</span>
        </a>
        <a class="nav-link" href="../comissao/pg-comissoes.html">
            <i class="fab fa-paypal"></i>
            <span>Pagamentos</span>
        </a>
        <a class="nav-link" href="../clientes/clientes-lista.html" target="_self">
            <i class="fas fa-user-friends"></i>
            <span>Clientes</span>
        </a>
    `;
    if (!representante) {
        itensMenu.innerHTML += `        
        <a class="nav-link" href="../representantes/representantes-lista.html">
            <i class="fas fa-user"></i>
            <span>Representantes</span>
        </a>
        <a class="nav-link" href="../produtos/produtos-lista.html">
        <i class="fas fa-cubes"></i>
            <span>Produtos</span>
        </a>
        <a class="nav-link" href="../tipo-comissao/tipo-comissao-lista.html">
        <i class="fas fa-funnel-dollar"></i>
            <span>Tipo de Comissão</span>
        </a>
        <a class="nav-link" href="../usuario/usuarios-lista.html">
        <i class="fas fa-user-shield"></i>
            <span>Usuarios</span>
        </a>
        <a class="nav-link" href="../permissao/permissao-lista.html">
        <i class="fas fa-shield-alt"></i>
            <span>Permissões</span>
        </a>
        <a class="nav-link" href="../grupos-empresa/grupos-empresa-lista.html">
        <i class="fas fa-layer-group"></i>
            <span>Grupos Empresas</span>
        </a>
        `;
    }
    itensMenu.innerHTML += `
            <a class="nav-link" href = "#" onclick = "usuario.logout()" >
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span>
        </a>
            `;
})();