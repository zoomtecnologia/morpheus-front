const servico = {
    async buscarSolicitacaoPorCodigo(codigoSolicitacao) {
        return requisicao.requisitarMetodoGET(`/comissoes/${codigoSolicitacao}/solicitacao-por-codigo`);
    },
    async gerarPayloadPix(payload) {
        return requisicao.requisitarMetodoPOST("/payload-pix", payload, false);
    }
};
const controle = {
    formatoMoeda: {
        pais: 'pt-BR',
        stilo: { style: 'currency', currency: 'BRL' }
    },
    async gerarPayloadPix(payload) {
        const resposta = await (await servico.gerarPayloadPix(payload)).text();
        return resposta;
    },
    async buscarSolicitacaoPorCodigo() {
        const loadingImage = document.querySelector("#loading-image");
        loadingImage.classList.remove("d-none");
        const codigoSolicitacao = document.querySelector("#codigoSolicitacao");
        const valorSolicitacao = document.querySelector("#valorSolicitacao");
        const representante = document.querySelector("#representante");
        const resposta = await (await servico.buscarSolicitacaoPorCodigo(comissaoId)).json();
        const totalSolicitacao = resposta.totalSolicitacao;
        const nomeRepresentante = resposta.nomeRepresentante;
        const chavePix = resposta.chavePix;
        const cidade = resposta.cidade;
        const documentoRepresentante = resposta.documentoRepresentante;
        const payloadPIX = await this.gerarPayloadPix({
            chavePix: chavePix,
            nomeTitular: nomeRepresentante,
            cidadeTitular: cidade,
            totalSolicitacao: totalSolicitacao,
            codigoSolicitacao: resposta.codigoSolicitacao
        });
        $("#imagemPix").qrcode(payloadPIX);
        loadingImage.classList.add("d-none");
        codigoSolicitacao.textContent = resposta.codigoSolicitacao;
        valorSolicitacao.textContent = totalSolicitacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        representante.textContent = nomeRepresentante;
    }
};
controle.buscarSolicitacaoPorCodigo();