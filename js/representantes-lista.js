let filtroPaginacao = {
    page: 0,
    size: 10,
    pesquisa: "",
    totalElementos: 0,
    ordenacao: "nome",
    resetPage: function () {
        this.page = 0;
    }
};

let representanteService = {
    url: `/representantes`
    , async listarRepresentantes(filtroPaginacao) {
        return requisicao.requisitarMetodoGET(`${this.url}?page=${filtroPaginacao.page}&size=${filtroPaginacao.size}&pesquisa=${filtroPaginacao.pesquisa}&sort=${filtroPaginacao.ordenacao}`);
    },
    async bloquearRepresentante(documentoRepresentante, status) {
        return requisicao.requisitarMetodoPUT(`${this.url}/${documentoRepresentante}/bloquear`, status, false, true);
    },
    async bloquearCliente(documentoRepresentante, status) {
        return requisicao.requisitarMetodoPUT(`/clientes/${documentoRepresentante}/bloquear-por-representante`, status, false, true);
    }
};

let representanteControle = {
    totalElementos: 0,
    async listarRepresentantes(concatenar) {
        const lista = document.getElementById("lista");
        if (!concatenar) {
            lista.innerHTML = "";
        }
        const loading = document.getElementById("loading");
        loading.classList.remove("d-none");
        const promise = await representanteService.listarRepresentantes(filtroPaginacao);
        const json = await promise.json();
        const representantes = json.content;
        this.totalElementos = representantes.totalElements;
        filtroPaginacao.totalElementos = this.totalElementos;
        const labelTotalRepresentante = json.totalElements == 1 ? "representante" : "representantes";
        document.getElementById("totalElementos").textContent = `${json.totalElements} ${labelTotalRepresentante}`;
        let linhas = "";
        for (const representante of representantes) {
            const tipoRepresentante = representante.tipo == "C" ? "Comercial" : "Suporte";
            let status = representante.status == "A" ? "Ativo" : "Inativo";
            let corStatus = representante.status == "A" ? "badge badge-info" : "badge badge-danger";
            const corGeraReceita = representante.geraReceita ? "badge badge-success" : "badge badge-warning";
            const labelGeraReceita = representante.geraReceita ? "Gera Receita" : "Não Gerar Receita";
            if (representante.status == "B") {
                corStatus = "badge badge-danger";
                status = "Bloqueado";
            }
            let nivelRepresentante = "PADAWAN";
            if (representante.nivel == "J") {
                nivelRepresentante = "JEDI";
            } else if (representante.nivel == "M") {
                nivelRepresentante = "MESTRE-JEDI";
            }
            const statusBloqueado = representante.status == "B" ? "fa-lock" : "fa-lock-open";
            linhas += `
            <li class="list-group-item shadow mb-2 flex-column align-items-start fade-form">
                <div class="d-flex w-100 justify-content-between">
                <h6 class="mb-1 font-weight-bold" style="font-size: 10pt;">${representante.nome}</h6>
                <span class="font-weight-bold" style="font-size: 10pt;">${representante.documento}</span>
                                        </div>
                                        <p class="mb-1 font-weight-bold" style="font-size: 9pt;text-align: right;">
                                            ${representante.nomeSocial}
                                        </p>
                                        <hr>
                                        <small class="mb-1">Dia ${representante.diaPagamentoComissao} ${tipoRepresentante} - ${nivelRepresentante}</small>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small>Fone: ${representante.celular}</small>
                                        </div>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small>E-mail: ${representante.email}</small>
                                        </div>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small class="${corStatus} mr-2">${status}</small> <small class="${corGeraReceita}">${labelGeraReceita}</small>
                                        </div>
                                        <hr>
                                        <div class="d-flex w-100 justify-content-end">
                                            <button class="btn btn-grid-pg m-2" onclick="location.href='representantes.html?documento=${representante.documento}'" >
                                               <i class="fas fa-user-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-grid-pg m-2" onclick="representanteControle.bloquearRepresentante('${representante.documento}','${representante.status}')">
                                               <i class="fas ${statusBloqueado}"></i>
                                            </button>
                                        </div>
                                    </li>
            `;
        }
        if (concatenar) {
            lista.innerHTML += linhas;
        } else {
            lista.innerHTML = linhas;
        }
        loading.classList.add("d-none");
    },
    async bloquearRepresentante(documento, status) {
        let statusRepresetante = status == "B" ? "A" : "B";
        let statusPagamento = status == "B" ? "P" : "B";
        const resposta = await (await representanteService.bloquearRepresentante(documento, statusRepresetante)).text();
        const respostaCliente = await (await representanteService.bloquearCliente(documento, statusPagamento)).text();
        this.listarRepresentantes();
    }
};

window.onload = function () {
    filtroPaginacao.pesquisa = "";
    representanteControle.listarRepresentantes();
};

const campoPesquisa = document.getElementById("pesquisar");
campoPesquisa.addEventListener("keypress", evt => {
    if (evt.keyCode == 13) {
        filtroPaginacao.pesquisa = campoPesquisa.value;
        filtroPaginacao.resetPage();
        representanteControle.totalElementos = 0;
        representanteControle.listarRepresentantes();
        campoPesquisa.select();
    }
});

window.onscroll = () => {
    scrollPaginacao.scrollPagination(() => {
        if (representanteControle.totalElementos === filtroPaginacao.totalElementos) {
            return;
        }
        const loading = document.getElementById("load-scroll");
        loading.classList.remove("d-none");
        filtroPaginacao.page++;
        filtroPaginacao.pesquisa = campoPesquisa.value;
        representanteControle.listarRepresentantes(true).then(() => {
            loading.classList.add("d-none");
        });
    });
};


const buttonPesquisa = document.getElementById("buttonPesquisar");
buttonPesquisa.addEventListener("click", evt => {
    filtroPaginacao.pesquisa = campoPesquisa.value;
    filtroPaginacao.resetPage();
    representanteControle.totalElementos = 0;
    representanteControle.listarRepresentantes();
    campoPesquisa.focus();
    campoPesquisa.select();
});
