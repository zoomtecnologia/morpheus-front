const btEnviar = document.getElementById("btEnviar");
const inputTexto = document.getElementById("inputTexto");
const mensagens = document.getElementById("mensagens");

const mensagemClienteNaoCadastrado = [
  { mensagem: "Digite o CNPJ do cliente.", html: false,tipoCampo: "tel" },
  { mensagem: "Digite a razão Social", html: false,tipoCampo:"text" },
  { mensagem: "Digite o Regime tributário <br/> 1- Simples Nacional <br/> 2- Lucro presumido <br/> 3- Regime Normal", html: true,tipoCampo:"tel" },
  { mensagem: "Digite o Cep", html: false,tipoCampo:"tel" },
  { mensagem: "Digite o número", html: false,tipoCampo:"text" },
  { mensagem: "Tem complemento ? \n 1- Sim \n 2- Não", html: false,tipoCampo:"tel" },
  { mensagem: "Digite o Complemento", html: false,tipoCampo:"text" },
  { mensagem: "Digite o e-mail", html: false,tipoCampo:"email" },
  { mensagem: "Digite o número do celular", html: false,tipoCampo:"tel" },
  { mensagem: "Digite o nome do contato", html: false,tipoCampo:"text" },
];

function criarMensagemRepresentante() {
  const linha = document.createElement("div");
  linha.classList.add("row");
  linha.classList.add("text-right");
  const coluna = document.createElement("div");
  coluna.classList.add("col-md-12");
  coluna.classList.add("text-right");
  coluna.classList.add("espaco-mensagens");
  const conteudo = document.createElement("p");
  conteudo.classList.add("mensagem-representante");
  conteudo.textContent = inputTexto.value;
  coluna.appendChild(conteudo);
  linha.appendChild(coluna);
  mensagens.appendChild(linha);
}

function criarMensagemSistema(objetoMensagem) {
  const linha = document.createElement("div");
  linha.classList.add("row");
  linha.classList.add("text-left");
  const coluna = document.createElement("div");
  coluna.classList.add("col-md-12");
  coluna.classList.add("text-left");
  coluna.classList.add("espaco-mensagens");
  const conteudo = document.createElement("p");
  conteudo.classList.add("mensagem-sistema");
  if (!objetoMensagem.html) {
    conteudo.textContent = objetoMensagem.mensagem;
  } else {
    conteudo.innerHTML = objetoMensagem.mensagem;
  }
  inputTexto.setAttribute("type",objetoMensagem.tipoCampo); 
  coluna.appendChild(conteudo);
  linha.appendChild(coluna);
  mensagens.appendChild(linha);
}

const formulario = document.getElementById("formulario");

let indeceElementos = 0;
formulario.addEventListener("submit", evt => {
  evt.preventDefault();
  criarMensagemRepresentante();
  indeceElementos++;
  criarMensagemSistema(mensagemClienteNaoCadastrado[indeceElementos]);
  const button = document.createElement("button");
  mensagens.appendChild(button);
  button.focus();
  mensagens.removeChild(button);
  inputTexto.focus();
  inputTexto.value = "";
});


window.onload = function () {
  criarMensagemSistema(mensagemClienteNaoCadastrado[0]);
};