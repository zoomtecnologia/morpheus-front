const formulario = document.querySelector("#formulario");
const usuarioNome = document.querySelector("#usuarioNome");
const usuarioEmail = document.querySelector("#usuarioEmail");
const usuarioSenha = document.querySelector("#usuarioSenha");
const usuarioPermissao = document.querySelector("#usuarioPermissao");
const blocoStatusUsuario = document.querySelector("#blocoStatusUsuario");
const usuarioStatus = document.querySelector("#usuarioStatus");
const usuarioRepresentante = document.querySelector("#usuarioRepresentante");

const usuarioControle = {
    codigoUsuario: 0,
    usuario: {},
    alterar: false,
    async salvarUsuario() {
        try {
            delete this.usuario.status;
            const resposta = await (await usuarioServico.salvar(this.usuario)).json();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            swal("Usuario salvo com sucesso", "Morpheus", "success");
            this.limparUsuario();
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
    },
    async alterarUsuario() {
        try {
            const resposta = await (await usuarioServico.alterar(this.codigoUsuario, this.usuario)).text();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            swal("Usuario salvo com sucesso", "Morpheus", "success");
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
    },
    async buscarUsuario() {
        const resposta = await (await usuarioServico.buscarUsuarioPorCodigo(this.codigoUsuario)).json();
        this.preencherTela(resposta);
    },
    async listarPermissoes() {
        const resposta = await (await permissaoServico.listarPermissoes(true)).json();
        let linhas = "";
        for (let permissao of resposta) {
            linhas += `<option value="${permissao.codigo}">${permissao.nome}</option>`;
        }
        usuarioPermissao.innerHTML = linhas;
    },
    async listarRepresentantes() {
        const resposta = await (await usuarioServico.listarRepresentanteResumido()).json();
        let linhas = "<option value=''>Selecione um representante</option>";
        for (let representante of resposta) {
            linhas += `<option value="${representante.documento}">${representante.nome}</option>`;
        }
        usuarioRepresentante.innerHTML = linhas;
    },
    limparUsuario() {
        this.usuario = null;
        this.usuario = {};
        usuarioNome.value = "";
        usuarioEmail.value = "";
        usuarioSenha.value = "";
        usuario.representante = "";
        usuarioPermissao.value = "";
        usuarioRepresentante.value = "";
    },
    preencherTela(resposta) {
        this.codigoUsuario = resposta.codigo;
        usuarioNome.value = resposta.nome;
        usuarioEmail.value = resposta.email;
        usuarioSenha.value = resposta.senha;
        usuarioRepresentante.value = resposta.representante.documento;
        usuarioPermissao.value = resposta.permissao.codigo;
        usuarioEmail.disabled = true;
        usuarioPermissao.disabled = true;
        blocoStatusUsuario.classList.remove("d-none");
        usuarioStatus.value = resposta.status;
    },
    mostrarInformacoes() {
        const parametro = window.location.search.slice(1);
        const usuarioID = parametro.replace(/codigo=/, "");
        this.listarPermissoes();
        this.listarRepresentantes();
        if (!usuarioID) {
            this.alterar = false;
            return;
        }
        this.alterar = true;
        this.codigoUsuario = usuarioID;
        this.buscarUsuario();
    }

};

formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    usuarioControle.usuario.nome = usuarioNome.value;
    usuarioControle.usuario.email = usuarioEmail.value;
    usuarioControle.usuario.senha = usuarioSenha.value;
    usuarioControle.usuario.permissao = usuarioPermissao.value;
    usuarioControle.usuario.status = usuarioStatus.value;
    if (usuarioControle.alterar) {
        usuarioControle.usuario.representante = { documento: usuarioRepresentante.value };
        usuarioControle.alterarUsuario();
        return;
    }
    usuarioControle.usuario.documentoRepresentante = usuarioRepresentante.value;
    usuarioControle.salvarUsuario();
});