const lista = document.querySelector("#lista");
const loading = document.querySelector("#loading");

const usuarioControle = {
    async listarUsuarios() {
        let resposta = [];
        try {
            loading.classList.remove("d-none");
            resposta = await (await usuarioServico.listarUsuarios()).json();
        } catch (error) {
            swal("Não foi possivel carregar as informações", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        loading.classList.add("d-none");
        let linhas = "";
        for (const usuario of resposta) {
            const status = usuario.status == "A" ? "Ativo" : "Inativo";
            const badgeStatus = usuario.status == "A" ? "badge badge-success" : "badge badge-danger";
            linhas += `<li class="list-group-item shadow mb-2 flex-column align-items-start">
             <div class="d-flex w-100 justify-content-between">
                <h6 class="mb-1 font-weight-bold" style="font-size: 14pt;">
                    ${usuario.nome}
                </h6>                 
                <small>Representante: ${usuario.representante.nome}</small>
             </div>
             <h5 class="mt-1 font-weight-bold" style="text-align:right;">
                <span class="${badgeStatus}">${status}</span>
             </h5>
             <h5 class="mt-1 font-weight-bold" style="text-align:right;">
                <small>${usuario.email}</small>
             </h5>
             <hr>
             <div class="d-flex w-100 justify-content-start">
                 Cadastro: ${usuario.dataCadastro}
             </div>
             <div class="d-flex w-100 justify-content-start">
                 Ultimo acesso: ${usuario.dataAcesso ? usuario.dataAcesso : ""}
             </div>
             <hr>
             <div class="d-flex w-100 justify-content-end">
                 <button class="btn btn-grid-pg m-2" onclick="location.href='usuarios.html?codigo=${usuario.codigo}'">
                     <span id="" class="spinner-grow spinner-grow-sm d-none"
                         role="status" aria-hidden="true"></span>
                     <i class="fas fa-edit"></i>
                 </button>
             </div>
         </li>`;
        }
        lista.innerHTML = linhas;
    }

};