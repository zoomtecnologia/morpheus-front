const usuarioServico = {
    async listarUsuarios() {
        return requisicao.requisitarMetodoGET("/usuarios?resumido=true");
    },
    async buscarUsuarioPorCodigo(codigo) {
        return requisicao.requisitarMetodoGET(`/usuarios/${codigo}?resumido=true`);
    },
    async salvar(usuario) {
        return requisicao.requisitarMetodoPOST("/usuarios", usuario, false);
    },
    async alterar(codigo, usuario) {
        delete usuario.email;
        delete usuario.documentoRepresentante;
        delete usuario.permissao;
        return requisicao.requisitarMetodoPUT(`/usuarios/${codigo}`, usuario, false, false);
    },
    async listarRepresentanteResumido(){
        return requisicao.requisitarMetodoGET("/representantes/todos?resumido=true");
    }
};