const tipoComissaoServico = {
    async listarTipoComissoes() {
        return requisicao.requisitarMetodoGET("/tipo-comissoes");
    }
};

const tipoComissaoControle = {
    async listarTipoComissoes() {
        const loading = document.getElementById("loading");
        loading.classList.remove("d-none");
        const listaTipoComissao = document.getElementById("listaTipoComissao");
        listaTipoComissao.innerHTML = "";
        const resposta = await (await tipoComissaoServico.listarTipoComissoes()).json();
        loading.classList.add("d-none");
        let linhas = "";
        for (const tipoComissao of resposta) {
            const badgeStatus = tipoComissao.status == "A" ? "success" : "danger";
            const textoStatus = tipoComissao.status == "A" ? "Ativo" : "Inativo";
            const badgeTipo = tipoComissao.tipo == "I" ? "primary" : "secondary";
            const textoTipo = tipoComissao.tipo == "I" ? "Instalação" : "Mensalidade";
            linhas += `
            <li class="list-group-item shadow mb-2 flex-column align-items-start">
            <div class="d-flex w-100 justify-content-start">
                <h6 class="mb-1 font-weight-bold" style="font-size: 10pt;">
                </h6>
                <span class="font-weight-bold mb-3" style="font-size: 15pt;">${tipoComissao.nome}</span>
            </div>
            <hr>
            <div class="row d-flex justify-content-between">
                <div class="col-sm-4 text-left">
                    <label>Comissão Representante</label>
                    <h5 class="font-weight-bold text-warning" style="text-align:left;">
                        ${tipoComissao.porcentagemRepresentante}%
                    </h5>
                </div>
                <div class="col-sm-4 text-left">
                    <label>Comissão Zoom</label>
                    <h5 class="font-weight-bold text-primary" style="text-align:left;">
                        ${tipoComissao.porcentagemZoom}%
                    </h5>
                </div>
            </div>
            <div class="row d-flex justify-content-between p-3 mb-2">
                <div class="col-xs-4 text-left mt-1 mb-2 " style="font-size: 10pt;">
                    Valor Mínimo:  R$${tipoComissao.valorMinimo}
                </div>
                <div class="col-xs-4 text-left mt-1 mb-2 " style="font-size: 10pt;">
                    Sem Comissão:  R$${tipoComissao.valorSemComissao}
                </div>
            </div>
            <div class="d-flex justify-content-start">
                <span class="badge badge-${badgeStatus} mr-2">${textoStatus}</span>
                <span class="badge badge-${badgeTipo} mr-2">${textoTipo}</span>
            </div>
            <hr>
            <div class="d-flex w-100 justify-content-end">
                <button class="btn btn-grid-pg m-2"
                    onclick="location.href='tipo-comissao.html?tipo-comissao=${tipoComissao.codigo}'">
                    <span id="" class="spinner-grow spinner-grow-sm d-none"
                        role="status" aria-hidden="true"></span>
                    <i class="fas fa-edit"></i>
                </button>
            </div>
        </li>`;
        }
        listaTipoComissao.innerHTML = linhas;
    }
};