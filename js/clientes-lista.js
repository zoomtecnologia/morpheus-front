let filtroPaginacao = {
    page: 0,
    size: 10,
    pesquisa: "",
    totalElementos: 0,
    ordenacao: "razaoSocial",
    representante: "",
    resetPage: function () {
        this.page = 0;
        this.camposFiltro = "";
    },
    camposFiltro: ""
};

let clienteServico = {
    url: "/clientes",
    listarClientes: async function (filtroPaginacao) {
        const pesquisa = filtroPaginacao.pesquisa;
        const urlListaCliente = `${this.url}?page=${filtroPaginacao.page}&size=${filtroPaginacao.size}&representante=${filtroPaginacao.representante}&pesquisa=${pesquisa}&sort=${filtroPaginacao.ordenacao}${filtroPaginacao.camposFiltro}`;
        return requisicao.requisitarMetodoGET(urlListaCliente);
    },
    async listarSeguimentos() {
        return requisicao.requisitarMetodoGET(`${this.url}/seguimentos`);
    },
    async listarRepresentantes() {
        return requisicao.requisitarMetodoGET("/representantes/todos");
    },
    buscarCliente: async function (cnpj) {
        return requisicao.requisitarMetodoGET(`${this.url}/${cnpj}`);
    },
    alterarStatusPagamento: async function (cnpj, status) {
        const urlAlterarSatausPagamento = `${this.url}/${cnpj}/status-pagamento`;
        return requisicao.requisitarMetodoPUT(urlAlterarSatausPagamento, status, false, true);
    },
    async listarStatusPedidos() {
        return requisicao.requisitarMetodoGET("/pedidos/status-pedido");
    },
    async listarGruposEmpresa() {
        return requisicao.requisitarMetodoGET("/grupo-empresas");
    }
};

let clienteControle = {
    totalElementos: 0
    , alterarStatusPagamento: async function (cnpj, status, indece) {
        const load = document.getElementById(`load-${indece}`);
        const buttonStatus = document.getElementById(`button-status-${indece}`);
        if (status == "V") {
            status = "P";
        } else if (status == "P") {
            status = "V";
        }
        load.classList.remove("d-none");
        buttonStatus.setAttribute("disabled", "disabled");
        const resposta = await (await clienteServico.alterarStatusPagamento(cnpj, status)).text();
        filtroPaginacao.pesquisa = cnpj;
        filtroPaginacao.resetPage();
        clienteControle.totalElementos = 0;
        this.listarClientes();
        document.getElementById("pesquisar").value = cnpj;
        load.classList.add("d-none");
        buttonStatus.removeAttribute("disabled");
    },
    listarClientes: async function (concatena) {
        const listaCliente = document.getElementById("listaCliente");
        if (!concatena) {
            listaCliente.innerHTML = "";
        }
        const loading = document.getElementById("loading");
        loading.classList.remove("d-none");
        if (usuario.permissoes.representante) {
            filtroPaginacao.representante = usuario.representante;
        }
        let resposta = [];
        try {
            resposta = await (await clienteServico.listarClientes(filtroPaginacao)).json();
        } catch (error) {
            loading.classList.add("d-none");
            swal("Não foi possivel carregar as informações", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        const clientes = !resposta.content ? [] : resposta.content;
        filtroPaginacao.totalElementos = !resposta.totalElements ? 0 : resposta.totalElements;
        const labelTotalCliente = !resposta.totalElements ? "cliente" : resposta.totalElements == 1 ? "cliente" : "clientes";
        document.getElementById("totalElementos").textContent = `${!resposta.totalElements ? 0 : resposta.totalElements} ${labelTotalCliente}`;
        let linhas = "";
        let indece = 0;
        for (let cliente of clientes) {
            clienteControle.totalElementos++;
            indece = clienteControle.totalElementos;
            const status = cliente.statusPagamento;
            let labelStatus = status == "P" ? "Pago" : "Vencido";
            let statusPagamento = status == "P" ? "btn-grid-pg" : "btn-grid-vc";
            const statusBloqueado = status == "B" ? "fa-lock" : "fa-lock-open";
            const statusBloqueadoAlteracao = status == "B" ? "V" : "B";
            let corStatusPagamento = status == "P" ? "badge badge-success" : "badge badge-warning";
            if (status == "B") {
                corStatusPagamento = "badge badge-danger";
                labelStatus = "Bloqueado";
                statusPagamento = "btn-grid-bloqueado";
            }
            const aceitouContrato = cliente.aceiteContrato == "A" ? "Contrato Aceito" : "Contrato Pendente";
            const corStatusAceite = cliente.aceiteContrato == "A" ? "badge badge-info" : "badge badge-secondary";
            const permissao = usuario.permissoes.representante ? "d-none" : "";
            let desabilitarBotaoManutencaoMensalidade = cliente.statusPedido == "INSTALACAO PAGA" || cliente.statusPedido == "FINALIZADO" ? "" : "disabled";
            const desabilitarEdicaoCliente = cliente.statusPedido != "SEM PEDIDO" && usuario.permissoes.representante ? "disabled" : "";
            const endereco = `${cliente.endereco.logradouro} ${cliente.endereco.numero} - ${cliente.endereco.bairro} - ${cliente.endereco.cidade}/${cliente.endereco.estado}`;
            linhas += `
            <li class="list-group-item shadow mb-2
             flex-column align-items-start fade-form">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h6 class="mb-1 font-weight-bold" style="font-size: 10pt;">${cliente.razaoSocial}</h6>
                                            <span class="font-weight-bold"
                                                style="font-size: 10pt;">${cliente.cnpj}</span>
                                        </div>
                                        <p class="mb-1 font-weight-bold" style="font-size: 9pt;text-align: right;">
                                            ${cliente.nomeFantasia}
                                        </p>
                                        <hr>
                                        <small class="mb-1">
                                        <i class="fas fa-map-marker-alt"></i> <a href="https://www.google.com.br/maps/search/${endereco}" target="_blank">${endereco}</a>
                                        </small>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small><i class="fas fa-phone"></i> <a href="tel:${cliente.celular}">${cliente.celular}</a></small>
                                            <small><i class="fab fa-whatsapp ml-2"></i> <a href="https://api.whatsapp.com/send?phone=55${cliente.celular}&amp;text=Ol%C3%A1" target="_blank">${cliente.celular}</a></small>
                                        </div>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small><i class="far fa-envelope"></i> <a href="mailto:${cliente.email}?subject=Zoom Tecnologia&body=Olá ${cliente.razaoSocial}">${cliente.email}</a></small>
                                        </div>
                                        <div class="d-flex w-100 justify-content-start">
                                            <small class="${corStatusPagamento} mr-2" >${labelStatus}</small>
                                            <small class="${corStatusAceite} mr-2" >${aceitouContrato}</small>
                                            <small class="font-weight-bold">${cliente.statusPedido}</small>
                                        </div>
                                        <hr>
                                        <div class="d-flex w-100 justify-content-end">
                                            <button id="pedido-${indece}" class="btn btn-grid-pg m-2" data-toggle="tooltip" data-placement="top" title="Pedido" onclick="clienteControle.iniciarPedido(${indece},'${cliente.cnpj}','${cliente.email}')">
                                                <span id="load-pedido-${indece}" class="spinner-grow spinner-grow-sm d-none" role="status" aria-hidden="true"></span>
                                                <i class="fab fa-product-hunt"></i>
                                            </button>
                                            <button id="button-status-${indece}" class="${permissao} btn ${statusPagamento} m-2" onclick="clienteControle.alterarStatusPagamento('${cliente.cnpj}','${cliente.statusPagamento}',${indece})">
                                                <span id="load-${indece}" class="spinner-grow spinner-grow-sm d-none" role="status" aria-hidden="true"></span>
                                                <i class="far fa-money-bill-alt"></i>
                                            </button>
                                            <button class="btn btn-grid-pg m-2" onclick="location.href='clientes.html?cnpj=${cliente.cnpj}'" ${desabilitarEdicaoCliente}>
                                                <i class="fas fa-user-edit"></i>
                                            </button>
                                            <button class="btn btn-grid-pg m-2 ${permissao}" onclick="clienteControle.alterarStatusPagamento('${cliente.cnpj}','${statusBloqueadoAlteracao}',${indece})">
                                                <i class="fas ${statusBloqueado}"></i>
                                            </button>
                                            <button ${desabilitarBotaoManutencaoMensalidade} class="btn btn-grid-pg m-2 ${permissao}" onclick="location.href='../geracao-mensalidade/geracao-mensalidade.html?cnpj=${cliente.cnpj}'">
                                            <i class="fas fa-file-invoice-dollar"></i>
                                            </button>
                                        </div>
                                    </li>
            `;
        }
        if (concatena) {
            listaCliente.innerHTML += linhas;
        } else {
            listaCliente.innerHTML = linhas;
        }
        loading.classList.add("d-none");
    },
    async buscarCliente(cnpj) {
        const resposta = await (await clienteServico.buscarCliente(cnpj)).json();
        return resposta;
    },
    async listarSeguimentos() {
        const resposta = await (await clienteServico.listarSeguimentos()).json();
        const filtroSeguimento = document.getElementById("filtroSeguimento");
        let linhas = "<option value=''>Selecione um Seguimento</option>";
        for (const seguimento of resposta) {
            linhas += `<option value='${seguimento}'>${seguimento}</option>`;
        }
        filtroSeguimento.innerHTML = linhas;
    },
    async listarRepresentantes() {
        const resposta = await (await clienteServico.listarRepresentantes()).json();
        const filtroRepresentantes = document.getElementById("filtroRepresentantes");
        let linhas = "<option value=''>Selecione um Representante</option>";
        for (const representante of resposta) {
            linhas += `<option value='${representante.documento}'>${representante.documento} - ${representante.nome}</option>`;
        }
        filtroRepresentantes.innerHTML = linhas;
    },
    async listarStatusPedido() {
        const resposta = await (await clienteServico.listarStatusPedidos()).json();
        const filtroStatusPedido = document.getElementById("filtroStatusPedido");
        let linhas = "<option value=''>Selecione um status do pedido</option>";
        for (const statusPedido of resposta) {
            linhas += `<option value='${statusPedido.status}'>${statusPedido.status}</option>`;
        }
        filtroStatusPedido.innerHTML = linhas;
    },
    async listarGruposEmpresa() {
        const resposta = await (await clienteServico.listarGruposEmpresa()).json();
        const filtroGrupoEmpresa = document.querySelector("#filtroGrupoEmpresa");
        let linhas = "<option value=''>Selecione um grupo</option>";
        for (const grupoEmpresa of resposta) {
            linhas += `<option value='${grupoEmpresa.codigo}'>${grupoEmpresa.nome}</option>`;
        }
        filtroGrupoEmpresa.innerHTML = linhas;
    },
    montarFiltro() {
        filtroPaginacao.resetPage();
        clienteControle.totalElementos = 0;
        const filtroStatusPagamento = document.getElementById("filtroStatusPagamento").value;
        const filtroStatusCliente = document.getElementById("filtroStatusCliente").value;
        const filtroRegimeTributado = document.getElementById("filtroRegimeTributado").value;
        const filtroSeguimento = document.getElementById("filtroSeguimento").value;
        const filtroRepresentantes = document.getElementById("filtroRepresentantes").value;
        const contingencia = document.getElementById("contingencia").checked;
        const aceiteContrato = document.getElementById("aceiteContrato").checked;
        const diaPagamento = document.getElementById("diaPagamento").value;
        const filtroStatusPedido = document.getElementById("filtroStatusPedido").value;
        const filtroGrupoEmpresa = document.querySelector("#filtroGrupoEmpresa").value;
        if (!!filtroStatusPagamento) {
            filtroPaginacao.camposFiltro += `&statusPagamento=${filtroStatusPagamento}`;
        }
        if (!!filtroStatusCliente) {
            filtroPaginacao.camposFiltro += `&statusAtividade=${filtroStatusCliente}`;
        }
        if (!!filtroRegimeTributado) {
            filtroPaginacao.camposFiltro += `&regimeTributario=${filtroRegimeTributado}`;
        }
        if (!!filtroSeguimento) {
            filtroPaginacao.camposFiltro += `&seguimento=${filtroSeguimento}`;
        }
        if (!!filtroRepresentantes) {
            filtroPaginacao.camposFiltro += `&codigoRepresentante=${filtroRepresentantes}`;
        }
        if (contingencia) {
            filtroPaginacao.camposFiltro += `&contingencia=S`;
        }
        if (aceiteContrato) {
            filtroPaginacao.camposFiltro += `&aceiteContrato=A`;
        }
        if (!!diaPagamento) {
            filtroPaginacao.camposFiltro += `&diaVencimento=${diaPagamento}`;
        }
        if (!!filtroStatusPedido) {
            filtroPaginacao.camposFiltro += `&statusPedido=${filtroStatusPedido}`;
        }
        if (!!filtroGrupoEmpresa) {
            filtroPaginacao.camposFiltro += `&codigoGrupoEmpresa=${filtroGrupoEmpresa}`;
        }
    },
    iniciarPedido(indece, cnpj, email) {
        const pedido = { cliente: {} };
        pedido.cliente.cnpj = cnpj;
        pedido.cliente.email = email;
        const btPedido = document.getElementById(`pedido-${indece}`);
        const loadingPedido = document.getElementById(`load-pedido-${indece}`);
        btPedido.setAttribute("disabled", "disabled");
        loadingPedido.classList.remove("d-none");
        pedidoControle.buscarPedidoPorCliente(cnpj).then(resposta => {
            if (resposta.status) {
                pedidoControle.salvarPedido(pedido).then(pedido => {
                    btPedido.removeAttribute("disabled");
                    loadingPedido.classList.add("d-none");
                    location.href = `../pedidos/pedidos.html?pedido=${pedido.numero}`;
                });
                return;
            }
            btPedido.removeAttribute("disabled");
            loadingPedido.classList.add("d-none");
            location.href = `../pedidos/pedidos.html?pedido=${resposta.numero}`;
        });
    }
};

window.onload = function () {
    filtroPaginacao.pesquisa = "";
    clienteControle.listarClientes();
    $('[data-toggle="tooltip"]').tooltip();
};

const campoPesquisa = document.getElementById("pesquisar");
campoPesquisa.addEventListener("keypress", evt => {
    if (evt.keyCode == 13) {
        filtroPaginacao.pesquisa = campoPesquisa.value;
        filtroPaginacao.resetPage();
        clienteControle.totalElementos = 0;
        clienteControle.listarClientes();
        campoPesquisa.select();
    }
});

window.onscroll = () => {
    scrollPaginacao.scrollPagination(() => {
        if (clienteControle.totalElementos === filtroPaginacao.totalElementos) {
            return;
        }
        const loading = document.getElementById("load-scroll");
        loading.classList.remove("d-none");
        filtroPaginacao.page++;
        filtroPaginacao.pesquisa = campoPesquisa.value;
        clienteControle.listarClientes(true).then(() => {
            loading.classList.add("d-none");
        });
    });
};

const buttonPesquisa = document.getElementById("buttonPesquisar");
buttonPesquisa.addEventListener("click", evt => {
    filtroPaginacao.pesquisa = campoPesquisa.value;
    filtroPaginacao.resetPage();
    clienteControle.totalElementos = 0;
    buttonPesquisa.setAttribute("disabled", "disabled");
    clienteControle.listarClientes().then(() => {
        buttonPesquisa.removeAttribute("disabled");
    });
    campoPesquisa.focus();
    campoPesquisa.select();
});

const btFiltro = document.getElementById("btFiltro");
btFiltro.addEventListener("click", evt => {
    clienteControle.listarSeguimentos();
    const linhaRepresentante = document.getElementById("linhaRepresentante");
    const colunaContingencia = document.getElementById("colunaContingencia");
    const filtroGrupoEmpresa = document.querySelector("#filtroGrupoEmpresa");
    if (!usuario.permissoes.representante) {
        clienteControle.listarRepresentantes();
        clienteControle.listarGruposEmpresa();
        linhaRepresentante.classList.remove("d-none");
        colunaContingencia.classList.remove("d-none");
        filtroGrupoEmpresa.classList.remove("d-none");
    } else {
        linhaRepresentante.classList.add("d-none");
        colunaContingencia.classList.add("d-none");
        filtroGrupoEmpresa.classList.add("d-none");
    }
    clienteControle.listarStatusPedido();
});

const btFiltra = document.getElementById("btFiltra");
btFiltra.addEventListener("click", evt => {
    clienteControle.montarFiltro();
    clienteControle.listarClientes();
    $('#exampleModal').modal('hide');
});

