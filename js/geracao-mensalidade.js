const filtroComissao = {
    representante: "",
    cliente: "",
    status: "PENDENTE",
    periodo: false,
    data: false,
};

const geracaoMensalidadeServico = {
    async buscarPedidoPorCliente(cnpj) {
        return requisicao.requisitarMetodoGET(`/pedidos/cliente/${cnpj}`);
    },
    async listarComissoes() {
        let urlConsultaComissoes = `/comissoes/comissoes-status?cliente=${filtroComissao.cliente}&representante=${filtroComissao.representante}`;
        if (!filtroComissao.periodo) {
            urlConsultaComissoes += `&status=${filtroComissao.status}`;
        } else {
            urlConsultaComissoes += `&status=${filtroComissao.status}&periodo=${filtroComissao.periodo}`;
            if (filtroComissao.data) {
                urlConsultaComissoes += "&data=true";
            }
        }
        return requisicao.requisitarMetodoGET(urlConsultaComissoes);
    },
    async alterarStatusComissao(comissoes) {
        return requisicao.requisitarMetodoPUT("/comissoes/status-comissao", comissoes, false, false);
    },
    async salvarComissaoManutencao(comissao) {
        return requisicao.requisitarMetodoPOST("/comissoes/manutencao", comissao, false);
    },
    async alterarStatusPedidoPorCliente(cliente) {
        return requisicao.requisitarMetodoPUT(`/pedidos/${cliente}/status-pedido-por-cliente`, "FINALIZADO", false, true);
    }
};

const geracaoMensalidadeControle = {
    formatoMoeda: {
        pais: 'pt-BR',
        stilo: { style: 'currency', currency: 'BRL' }
    },
    pedido: {},
    comissoes: [],
    comissoesSelecionadas: [],
    marcaTodos: true,
    comissaoSelecionada: {},
    async buscarPedidoPorCliente(cnpj) {
        const resposta = await (await geracaoMensalidadeServico.buscarPedidoPorCliente(cnpj)).json();
        return resposta;
    },
    async listarComissoes() {
        filtroComissao.representante = this.pedido.documentoRepresentante;
        filtroComissao.cliente = this.pedido.cnpjCliente;
        const comissoes = await (await geracaoMensalidadeServico.listarComissoes()).json();
        if (comissoes.length == 0) {
            document.getElementById("btMarcaTodos").disabled = true;
        }
        this.comissoes = comissoes;
        const listarComissoes = document.getElementById("listaComissoes");
        let linhas = "";
        const liberarMarcacao = filtroComissao.status == "PAGO";
        let indeceComissao = 0;
        for (const comissao of comissoes) {
            const tipo = comissao.tipo == "INSTALACAO" ? "I" : "M";
            const badgeTipo = tipo == "I" ? "primary" : "secondary";
            const data = comissao.data ? comissao.data : "00/00/0000";
            const valor = comissao.valor.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            let badgeStatus = "warning";
            if (comissao.status == "LIBERADO") {
                badgeStatus = "success";
            } else if (comissao.status == "PAGO") {
                badgeStatus = "success";
            }
            let valorInstalacaoMensalidade = comissao.valorMensalidade.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            if (comissao.tipo == "INSTALACAO") {
                valorInstalacaoMensalidade = comissao.valorInstalacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            }
            const desabilitarCheckBox = liberarMarcacao ? "disabled" : "";
            document.getElementById("btMarcaTodos").disabled = liberarMarcacao;
            linhas += `<div class="list-group-item flex-column align-items-start">
            <div class="d-flex justify-content-between">
                <span class="badge badge-${badgeStatus}"
                    style="font-size: 8pt;width: 20px; height: 20px; border-radius: 50px;">
                </span>
                <span class="badge badge-${badgeTipo} align-items-center"
                    style="font-size: 9pt;width: 20px; height: 20px; border-radius: 3px; margin-left: -50px;">
                    ${tipo}
                </span>
                <span style="font-size: 10pt;">Venc.: ${data}</span>
                <div class="checkbox">
                    <label style="font-size: 1em">
                        <input type="checkbox" class="selecao" value="" ${desabilitarCheckBox} onclick="geracaoMensalidadeControle.selecionar(this,${indeceComissao})">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
                <span>${comissao.razaoSocial}</span>
                <span class="ml-5" style="font-size: 20pt;">${valorInstalacaoMensalidade}</span>
            </div>
            <div class="d-flex align-items-center justify-content-end">
                <span style="font-size: 10pt;">Comissão: ${valor}</span>
            </div>
            <div class="text-right ">
                <button type="button" class="btn btn-grid-pg m-2" ${desabilitarCheckBox}  data-toggle="modal" data-target="#modalConfirmacao" onclick="geracaoMensalidadeControle.selecionarComissao(${indeceComissao})">
                    <i class="fab fa-paypal"></i>
                </button>
            </div>
        </div>`;
            indeceComissao++;
        }
        listarComissoes.innerHTML = linhas;
    },
    async cancelarComissao() {
        if (this.comissoesSelecionadas.length == 0) {
            return;
        }
        const comissoesCanceladas = this.comissoesSelecionadas.map(comissao => {
            return { codigo: comissao.codigo, status: "CANCELADO" };
        });
        const resposta = await (await geracaoMensalidadeServico.alterarStatusComissao(comissoesCanceladas));
        this.mostrarComissoesPendentes();
    },
    async salvarComissaoManutencao() {
        let comissaoManutencao = {};
        comissaoManutencao.tipo = document.getElementById("tipoDaComissao").value == "I" ? "INSTALACAO" : "MENSALIDADE";
        comissaoManutencao.valorMensalidade = document.getElementById("valorDaComissao").value;
        comissaoManutencao.dataMensalidade = document.getElementById("dataComissao").value;
        comissaoManutencao.codigoClienteCnpj = this.pedido.cnpjCliente;
        comissaoManutencao.codigoRepresentanteDocumento = this.pedido.documentoRepresentante;
        comissaoManutencao.quantidadeParcela = document.getElementById("quantidadeParcelas").value;
        comissaoManutencao.pedido = this.pedido;
        const resposta = await (await geracaoMensalidadeServico.salvarComissaoManutencao(comissaoManutencao)).text();
        this.mostrarComissoesPendentes();
        $('#exampleModal').modal('hide');
    },
    async alterarStatusComissao() {
        const comissaoStatus = { codigo: this.comissaoSelecionada.codigo, status: "LIBERADO" };
        const resposta = await (await geracaoMensalidadeServico.alterarStatusComissao([comissaoStatus]));
        this.comissaoSelecionada = {};
        this.mostrarComissoesPendentes();
        if (this.pedido.status != "FINALIZADO") {
            geracaoMensalidadeServico.alterarStatusPedidoPorCliente(this.pedido.cnpjCliente);
        }
    },
    selecionarComissao(indeceComissao) {
        const comissao = this.comissoes[indeceComissao];
        this.comissaoSelecionada = comissao;
        const valorAserConfirmadoParaPagamento = document.getElementById("valorAserConfirmadoParaPagamento");
        const modalParcela = document.getElementById("modalParcela");
        const valorConfirmacaoComissao = document.getElementById("valorConfirmacaoComissao");
        let valor = comissao.valorInstalacao;
        let textoTipoComissao = "Instalação";
        if (comissao.tipo == "MENSALIDADE") {
            valor = comissao.valorMensalidade;
            textoTipoComissao = "Mensalidade";
        }
        valorAserConfirmadoParaPagamento.textContent = textoTipoComissao +" "+ valor.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        valorConfirmacaoComissao.textContent = comissao.valor.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        modalParcela.textContent = comissao.data;
    },
    verificarSeExisteAlgumaComissaoSelecionada() {
        const inputsSelecionados = Array.from(document.querySelectorAll(".selecao"));
        const comissaoSelecionada = inputsSelecionados.some(elemento => elemento.checked);
        return comissaoSelecionada;
    },
    habilidarBotao() {
        const inputSelecionados = this.verificarSeExisteAlgumaComissaoSelecionada();
        if (!inputSelecionados) {
            document.getElementById("btMarcaTodos").textContent = "Marcar Todos";
            this.marcaTodos = true;
        }
    },
    selecionarTodos() {
        const inputsSelecionados = document.querySelectorAll(".selecao");
        if (this.marcaTodos) {
            inputsSelecionados.forEach(input => input.checked = true);
            this.comissoesSelecionadas = this.comissoes;
            document.getElementById("btMarcaTodos").textContent = "Desmarcar Todos";
            this.marcaTodos = false;
            return;
        }
        inputsSelecionados.forEach(input => input.checked = false);
        document.getElementById("btMarcaTodos").textContent = "Marcar Todos";
        this.marcaTodos = true;
        this.comissoesSelecionadas = [];
    },
    selecionar(input, indeceComissao) {
        const comissao = this.comissoes[indeceComissao];
        if (input.checked) {
            this.comissoesSelecionadas.push(comissao);
        } else {
            const indeceNaComissaoSelecionada = this.comissoesSelecionadas.indexOf(comissao);
            this.comissoesSelecionadas.splice(indeceNaComissaoSelecionada, 1);
        }
        this.habilidarBotao();
    },
    novaCobranca() {
        const tipoDaComissao = document.getElementById("tipoDaComissao");
        document.getElementById("nomeDoClienteModal").textContent = this.pedido.razaoSocialCliente;
        tipoDaComissao.value = "";
        document.getElementById("valorDaComissao").value = null;
        var today = new Date();
        today = today.toISOString().split('T')[0];
        document.getElementById("dataComissao").min = today;
        document.getElementById("quantidadeParcelas").value = 1;
    },
    preencherInformacoesDoCliente() {
        const parametro = window.location.search.slice(1);
        const clienteId = parametro.replace(/cnpj=/, "");
        if (!clienteId) {
            location.href = "../clientes/clientes-lista.html";
            return;
        }
        this.buscarPedidoPorCliente(clienteId).then(resposta => {
            this.pedido = resposta;
            document.getElementById("nomeDoCliente").textContent = resposta.razaoSocialCliente;
            geracaoMensalidadeControle.mostrarComissoesPendentes();
        });
    },
    mostrarComissoesPagas() {
        filtroComissao.status = "PAGO";
        filtroComissao.periodo = true;
        filtroComissao.data = false;
        this.listarComissoes();
    },
    mostrarComissoesPendentes() {
        filtroComissao.status = "PENDENTE";
        filtroComissao.periodo = true;
        filtroComissao.data = false;
        this.listarComissoes();
    },
    mostrarComissoesVencidas() {
        filtroComissao.status = "PENDENTE";
        filtroComissao.periodo = true;
        filtroComissao.data = true;
        this.listarComissoes();
    }
};

const btPago = document.getElementById("btPago");
btPago.addEventListener("click", evt => geracaoMensalidadeControle.mostrarComissoesPagas());
const btPendente = document.getElementById("btPendente");
btPendente.addEventListener("click", evt => geracaoMensalidadeControle.mostrarComissoesPendentes());
const btVencido = document.getElementById("btVencido");
btVencido.addEventListener("click", evt => geracaoMensalidadeControle.mostrarComissoesVencidas());
const btNovaComissao = document.getElementById("btNovaComissao");
btNovaComissao.addEventListener("click", evt => geracaoMensalidadeControle.novaCobranca());
const btCancelamento = document.getElementById("btCancelamento");
btCancelamento.addEventListener("click", evt => geracaoMensalidadeControle.cancelarComissao());
const btMarcaTodos = document.getElementById("btMarcaTodos");
btMarcaTodos.addEventListener("click", evt => geracaoMensalidadeControle.selecionarTodos());
const btConfirmaComissao = document.getElementById("btConfirmaComissao");
btConfirmaComissao.addEventListener("click", evt => {
    geracaoMensalidadeControle.alterarStatusComissao();
    $("#modalConfirmacao").modal("hide");
});

const formularioComissao = document.getElementById("formularioComissao");
formularioComissao.addEventListener("submit", evt => {
    evt.preventDefault();
    geracaoMensalidadeControle.salvarComissaoManutencao();
});
const tipoDaComissao = document.getElementById("tipoDaComissao");
tipoDaComissao.addEventListener("change", evt => {
    const painelQuantidadeParcelas = document.getElementById("painelQuantidadeParcelas");
    const quantidadeParcelas = document.getElementById("quantidadeParcelas");
    painelQuantidadeParcelas.classList.add("d-none");
    quantidadeParcelas.required = false;
    if (tipoDaComissao.value == "M") {
        quantidadeParcelas.required = true;
        painelQuantidadeParcelas.classList.remove("d-none");
    }
});