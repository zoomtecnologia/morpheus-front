let grupoEmpresaServico = {
    PUT: "PUT",
    POST: "POST",
    GET: "GET",
    requestInit: {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': usuario.autorizacao
        },
        body: {}
    },
    listarGrupoEmpresa: async function () {
        const url = `${servidor.URL_API}/grupo-empresas`;
        delete this.requestInit.body;
        this.requestInit.method = this.GET;
        return fetch(url,this.requestInit);
    }
};

let grupoEmpresaControle = {
    listarGrupoEmpresa: async function () {
        const grupoDeEmpresas = await (await grupoEmpresaServico.listarGrupoEmpresa()).json();
        return grupoDeEmpresas;
    }
};

