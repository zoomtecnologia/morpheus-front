const valorPedido = document.getElementById("valorPedido");

const itensService = {
    async listarItens() {
        return requisicao.requisitarMetodoGET("/itens");
    },
    async listarItensPedido(numeroPedido) {
        return requisicao.requisitarMetodoGET(`/pedidos/${numeroPedido}/itens`);
    },
    async salvarItemPedido(item, numeroPedido) {
        return requisicao.requisitarMetodoPOST(`/pedidos/${numeroPedido}/itens`, item, false);
    },
    async alterarItem(item, numeroPedido) {
        return requisicao.requisitarMetodoPUT(`/pedidos/${numeroPedido}/itens`, item, false, false);
    }
};

const tipoDeComissaoServico = {
    async listarTipoDeComissoes() {
        return requisicao.requisitarMetodoGET(`/tipo-comissoes`);
    }
};

const pedidoService = {
    async salvarPedido(pedido) {
        return requisicao.requisitarMetodoPOST(`/pedidos`, pedido, false);
    },
    async alterarPedido(pedido, numeroPedido) {
        delete pedido.numero;
        delete pedido.documentoRepresentante;
        delete pedido.cliente;
        delete pedido.representante;
        delete pedido.data;
        return requisicao.requisitarMetodoPUT(`/pedidos/${numeroPedido}`, pedido, false, false);
    },
    async buscarPedido(numeroPedido) {
        return requisicao.requisitarMetodoGET(`/pedidos/${numeroPedido}`);
    },
    async buscarPedidoPorCliente(cnpj) {
        return requisicao.requisitarMetodoGET(`/pedidos/cliente/${cnpj}`);
    },
    async alterarStatusPedido(numeroPedido, status) {
        return requisicao.requisitarMetodoPUT(`/pedidos/${numeroPedido}/status-pedido`, status, false, true);
    },
    async enviarContratoPorEmail(numeroPedido) {
        return requisicao.requisitarMetodoPOST(`/pedidos/${numeroPedido}/enviar-contrato`, "", true);
    },
    async enviarEmailDeStatusPedidoParaCliente(numeroPedido) {
        return requisicao.requisitarMetodoPOST(`/pedidos/${numeroPedido}/enviar-email-status-pedido`, "", true);
    }
};

const pedidoControle = {
    numeroPedido: 0
    , pedido: {}
    , cliente: {}
    , totalPedido: 0
    , itensPedido: []
    , recalcularTotalPedido() {
        this.totalPedido = 0;
        const checkes = document.querySelectorAll(".selecao");
        let indece = 0;
        for (const checkbox of checkes) {
            if (!checkbox.checked) {
                this.itensPedido[indece].status = "I";
                this.itensPedido[indece].quantidade = 1;
                indece++;
                continue
            }
            const inputValor = document.getElementById(`valor-${indece}`);
            const inputQuantidade = document.getElementById(`quantidade-${indece}`);
            let quantidade = 1;
            const valorAtualDoItem = parseFloat(inputValor.value);
            if (!!inputQuantidade) {
                quantidade = parseFloat(inputQuantidade.value);
            }
            const totalItem = valorAtualDoItem * quantidade;
            this.itensPedido[indece].status = "A";
            this.itensPedido[indece].valor = valorAtualDoItem;
            this.itensPedido[indece].quantidade = quantidade;
            this.totalPedido += totalItem;
            indece++;
        }
        valorPedido.textContent = this.totalPedido.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    },
    async buscarPedidoPorCliente(cnpj) {
        return await (await pedidoService.buscarPedidoPorCliente(cnpj)).json();
    },
    async listarItens() {
        let itens;
        const statusPedidoAberto = this.pedido.status == "ABERTO";
        if (statusPedidoAberto) {
            itens = await (await itensService.listarItens()).json();
        } else {
            itens = await (await itensService.listarItensPedido(this.numeroPedido)).json();
        }
        const listaItens = document.getElementById("listaItens");
        let linha = "";
        this.totalPedido = 0;
        let indece = 0;
        this.itensPedido = itens;

        for (const item of itens) {
            if (statusPedidoAberto && item.status == "I") {
                continue;
            }
            let desabilitarPedidoLancado = this.pedido.status == "ABERTO" || !usuario.permissoes.representante ? "" : "disabled";
            const valorCampoQuantidade = item.quantidade ? item.quantidade : 1;
            const input = item.itemUnico ? "" : `Quantidade: <input id='quantidade-${indece}' ${desabilitarPedidoLancado} min="1" class='form-control' type='number' style='text-align:center' value='${valorCampoQuantidade}' onchange='pedidoControle.recalcularTotalPedido()'/>`;
            let disabilitarCampoDeTexto = "";
            let disabilitarCheckBox = "";
            let selecaoDesabilitada = "";
            if (item.descricao != "Mensalidade") {
                this.totalPedido += item.valor * valorCampoQuantidade;
                disabilitarCampoDeTexto = desabilitarPedidoLancado;
            } else {
                const inputMensalidade = document.getElementById("mensalidade");
                if (!inputMensalidade.value) {
                    inputMensalidade.value = item.valor;
                }
                if (usuario.permissoes.representante) {
                    inputMensalidade.min = item.valorMinimo;
                }
                disabilitarCampoDeTexto = "disabled";
                selecaoDesabilitada = "";
                desabilitarPedidoLancado = "";
            }
            if (item.status == "A" && item.descricao != "Mensalidade") {
                selecaoDesabilitada = "checked";
            }
            if ((usuario.permissoes.representante && !item.opcional) || item.descricao == "Mensalidade") {
                disabilitarCheckBox = "disabled";
            }
            document.getElementById("observacao").disabled = usuario.permissoes.representante && this.pedido.status != "ABERTO";
            const codigo = item.codigo ? item.codigo : item.codigoItem;
            const valorMinimoDoItem = usuario.permissoes.representante ? item.valorMinimo : 0;
            linha += `
            <li href="#" class="list-group-item">
            <div class="container-fluid">
            <div class="row">               
                <div class="col-md-1"> 
                <div class="checkbox">
                    <label style="font-size: 1em">         
                    <input type="checkbox" class="selecao" value="" ${disabilitarCheckBox} ${selecaoDesabilitada}  onclick="pedidoControle.recalcularTotalPedido()">                  
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>  
                </div>            
                <div class="col-md-2">
                    <small class="font-weight-bold">${codigo}</small>
                </div>
                <div class="col-md-4">
                    <h5 class="mb-1">${item.descricao}</h5>
                </div>
                <div class="col-md-2">
                    Valor R$:<input type="number" id="valor-${indece}" min="${valorMinimoDoItem}" class="form-control total" ${desabilitarPedidoLancado} ${disabilitarCampoDeTexto} value="${item.valor}" style="text-align:center" onchange="pedidoControle.recalcularTotalPedido()"/>
                </div>
                <div class="col-md-2">
                    ${input}                
                </div>
            </div>
            </div>
                
            </li>
            `;
            indece++;
        }
        listaItens.innerHTML = linha;
        if (this.pedido.total == 0.0) {
            valorPedido.textContent = this.totalPedido.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        }
    },
    async salvarPedido(pedido) {
        pedido.documentoRepresentante = usuario.representante;
        pedido.tipoComissao = 1;
        pedido.total = pedidoControle.totalPedido;
        pedido.valorMensalidade = 150;
        pedido.obs = "";
        const pedidoSalvo = await (await pedidoService.salvarPedido(pedido)).json();
        return pedidoSalvo;
    },
    async alterarPedido(numero) {
        this.pedido.obs = document.getElementById("observacao").value;
        this.pedido.valorMensalidade = document.getElementById("mensalidade").value;
        const statusPedidoRepresentante = document.getElementById("statusPedidoRepresentante");
        const dataInstalacao = document.getElementById("dataInstalacao");
        const horaInstalacao = document.getElementById("horaInstalacao");
        const tipoComissao = document.getElementById("tipoComissao");
        if (!usuario.permissoes.representante) {
            if (!!dataInstalacao.value) {
                this.pedido.dataMarcacaoInstalacao = `${dataInstalacao.value}T${horaInstalacao.value}:00`;
            }
            if (!!tipoComissao.value) {
                this.pedido.tipoComissao = tipoComissao.value;
            }
            if (!!statusPedidoRepresentante.value) {
                this.pedido.status = statusPedidoRepresentante.value;
            }
        }
        this.pedido.total = this.totalPedido;
        const resposta = await (await pedidoService.alterarPedido(this.pedido, numero)).json();
        this.pedido = resposta;
        if (!!statusPedidoRepresentante.value) {
            document.getElementById("statusPedido").textContent = resposta.status;
            this.alterarStatusPedido(statusPedidoRepresentante.value);
        }
        if (!!resposta.dataMarcacaoInstalacao) {
            document.getElementById("instalacaoMarcada").textContent = "Instalação marcada: " + new Date(resposta.dataMarcacaoInstalacao).toLocaleString();
        }
        const mensalidade = document.getElementById("mensalidade");
        mensalidade.removeAttribute("disabled");
        if (usuario.permissoes.representante && this.pedido.status != "ABERTO") {
            mensalidade.setAttribute("disabled", "disabled");
        }
        dataInstalacao.value = null;
        horaInstalacao.value = null;
        statusPedidoRepresentante.value = "";
        tipoComissao.value = "";
    },
    async salvarItemPedido(numeroPedido) {
        this.recalcularTotalPedido();
        if (this.pedido.status != "ABERTO") {
            let itens = [];
            for (const item of this.itensPedido) {
                const itemSalvo = {};
                itemSalvo.codigoItem = item.codigoItem;
                itemSalvo.quantidade = item.quantidade;
                itemSalvo.valor = item.valor;
                itemSalvo.status = item.status;
                itens.push(itemSalvo);
            }
            const resposta = await (await itensService.alterarItem(itens, numeroPedido)).text();
            return;
        }
        let itens = [];
        for (const item of this.itensPedido) {
            const itemSalvo = {};
            itemSalvo.codigoItem = item.codigo;
            itemSalvo.quantidade = item.quantidade;
            itemSalvo.valor = item.valor;
            itemSalvo.status = item.status;
            itens.push(itemSalvo);
        }
        const resposta = await (await itensService.salvarItemPedido(itens, numeroPedido)).json();
        this.pedido.status = "LANCADO";
        document.getElementById("statusPedido").textContent = "LANÇADO";
    },
    async alterarStatusPedido(statusPedidoRepresentante) {
        const resposta = await (await pedidoService.alterarStatusPedido(this.numeroPedido, statusPedidoRepresentante)).text();
    },
    async buscarPedido(numeroPedido) {
        const pedido = await (await pedidoService.buscarPedido(numeroPedido)).json();
        return pedido;
    },
    async listarTipoDeComissoes() {
        const resposta = await (await tipoDeComissaoServico.listarTipoDeComissoes()).json();
        const tipoComissao = document.getElementById("tipoComissao");
        for (const tipoComissaoCorrente of resposta) {
            if(tipoComissaoCorrente.status == "I"){
                continue;
            }
            const option = document.createElement("option");
            option.value = tipoComissaoCorrente.codigo;
            option.textContent = `${tipoComissaoCorrente.nome} - ${tipoComissaoCorrente.porcentagemRepresentante} %`;
            tipoComissao.appendChild(option);
        }
    },
    async enviarContratoPorEmail() {
        const contratoPendente = !this.cliente.aceiteContrato;
        if (this.pedido.status == "LANCADO" && contratoPendente) {
            pedidoService.enviarContratoPorEmail(this.numeroPedido);
        }
    },
    async enviarEmailDeStatusPedidoParaCliente() {
        if (this.pedido.status == "INSTALACAO_MARCADA" || this.pedido.status == "INSTALACAO_FINALIZADA") {
            pedidoService.enviarEmailDeStatusPedidoParaCliente(this.numeroPedido);
        }
    },
    mostrarDataEHoraDaInstalacao() {
        const statusPedidoRepresentante = document.getElementById("statusPedidoRepresentante");
        statusPedidoRepresentante.addEventListener("change", () => {
            if (statusPedidoRepresentante.value == "INSTALACAO_MARCADA") {
                document.querySelectorAll(".grupo-esconder-data-instalacao").forEach(elemento => {
                    elemento.classList.remove("grupo-esconder-data-instalacao");
                    elemento.classList.add("grupo-mostrar-data-instalacao");
                });
                return;
            }
            document.querySelectorAll(".grupo-mostrar-data-instalacao").forEach(elemento => {
                elemento.classList.remove("grupo-mostrar-data-instalacao");
                elemento.classList.add("grupo-esconder-data-instalacao");
            });
        });
    },
    async lancarPedido() {
        const loading = document.getElementById("loading");
        loading.classList.remove("d-none");
        const btSalvarPedido = document.getElementById("btSalvarPedido");
        btSalvarPedido.setAttribute("disabled", "disabled");
        if (this.pedido.status == "ABERTO" || !usuario.permissoes.representante) {
            const respostaItem = await (await this.salvarItemPedido(this.numeroPedido));
        }
        this.alterarPedido(this.numeroPedido).then(() => {
            loading.classList.add("d-none");
            btSalvarPedido.removeAttribute("disabled");
            swal("Pedido Salvo", "", "success");
            this.listarItens();
            this.enviarContratoPorEmail();
            this.enviarEmailDeStatusPedidoParaCliente();
        });
    },
    confirmacaoAntesDeLancarPedido(confirmar) {
        swal({
            title: "PEDIDO",
            text: "Você realmente deseja salvar o pedido",
            icon: "warning",
            buttons: {
                cancel: "Não",
                defeat: "Sim"
            },
            dangerMode: true,
        }).then((willDelete) => {
            if (!willDelete) {
                return;
            }
            confirmar();
        });
    },
    abriManutencaoPedido() {
        location.href = "../pedidos/manutencao.html?pedido=" + this.numeroPedido;
    },
    mostrarInformacoesPedido() {
        const parametro = window.location.search.slice(1);
        const numeroPedido = parametro.replace(/pedido=/, "");
        if (!numeroPedido) {
            location.href = "../clientes/clientes-lista.html";
            return;
        }
        this.numeroPedido = numeroPedido;
        const dataLancamento = document.getElementById("dataLancamento");
        dataLancamento.textContent = "Data: " + new Date().toLocaleDateString();
        const loadCliente = document.getElementById("load-cliente");
        loadCliente.classList.remove("d-none");
        this.buscarPedido(numeroPedido).then(resposta => {
            pedidoControle.pedido = resposta;
            pedidoControle.cliente = resposta.cliente;
            document.getElementById("razaoSocial").textContent = resposta.cliente.razaoSocial;
            document.getElementById("cnpj").textContent = resposta.cliente.cnpj;
            document.getElementById("nomeFantasia").textContent = resposta.cliente.nomeFantasia;
            const endereco = `${resposta.cliente.endereco.logradouro} ${resposta.cliente.endereco.numero} - ${resposta.cliente.endereco.bairro} - ${resposta.cliente.endereco.cidade}/${resposta.cliente.endereco.estado}`;
            document.getElementById("endereco").textContent = endereco;
            document.getElementById("celular").textContent = `${resposta.cliente.celular}`;
            document.getElementById("email").textContent = `${resposta.cliente.email}`;
            document.getElementById("numeroPedido").textContent = `#${resposta.numero}`;
            document.getElementById("statusPedido").textContent = `${resposta.status}`;
            document.getElementById("observacao").value = resposta.obs;
            const mensalidade = document.getElementById("mensalidade");
            mensalidade.value = resposta.valorMensalidade;
            document.getElementById("valorPedido").textContent = resposta.total.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
            if (resposta.dataMarcacaoInstalacao) {
                document.getElementById("instalacaoMarcada").textContent = "Instalação marcada: " + new Date(resposta.dataMarcacaoInstalacao).toLocaleString();
            }
            loadCliente.classList.add("d-none");
            pedidoControle.listarItens();
            const informacoesComplementares = document.getElementById("informacoesComplementares");
            mensalidade.removeAttribute("disabled");
            if (usuario.permissoes.representante && this.pedido.status != "ABERTO") {
                mensalidade.setAttribute("disabled", "disabled");
                document.getElementById("btSalvarPedido").disabled = true;
            }
            if (!usuario.permissoes.representante && this.pedido.status != "INSTALACAO_PAGA" && !!this.cliente.aceiteContrato) {
                informacoesComplementares.classList.remove("d-none");
            }
        }).catch(error => {
            loadCliente.classList.add("d-none");
        });
        pedidoControle.listarTipoDeComissoes();
        this.mostrarDataEHoraDaInstalacao();
        const lacamentoDePedido = document.getElementById("lacamentoDePedido");
        lacamentoDePedido.addEventListener("submit", evt => {
            evt.preventDefault();
            this.confirmacaoAntesDeLancarPedido(() => this.lancarPedido());
        });
    }
};