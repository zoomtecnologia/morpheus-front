const usuarioServico = {
    async verificarEmail(email) {
        const verificaEmail = { email: email };
        return requisicao.requisitarMetodoPOST("/usuarios/verificar-email-cadastrado", verificaEmail, false);
    },
    async enviarEmail(email) {
        const verificaEmail = { email: email };
        return requisicao.requisitarMetodoPOST("/usuarios/enviar-email-redefinir-senha", verificaEmail, false);
    }
};

const usuarioControle = {
    email: "",
    async verificaEmail() {
        try {
            const resposta = await (await usuarioServico.verificarEmail(this.email)).json();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            usuarioServico.enviarEmail(this.email);
            swal("Email enviado com sucesso", "Morpheus", "success").then(() => {
                location.href= "../../index.html";
            });            
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
    }
};

const formulario = document.querySelector("#formulario");
const loginEmail = document.querySelector("#loginEmail");

formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    usuarioControle.email = loginEmail.value;
    usuarioControle.verificaEmail();
});