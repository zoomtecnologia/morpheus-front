const filtro = {
    representante: "",
    dataInicial: "",
    dataFinal: ""
};

const comissaoService = {
    async listarTotais() {
        const urlConsultaTotais = `/comissoes/totais-representante?representante=${filtro.representante}&dataInicial=${filtro.dataInicial}&dataFinal=${filtro.dataFinal}`;
        return requisicao.requisitarMetodoGET(urlConsultaTotais);
    },
    async listarComissoes(status) {
        const urlConsultaComissoes = `/comissoes/comissoes-status?representante=${filtro.representante}&status=${status}&dataInicial=${filtro.dataInicial}&dataFinal=${filtro.dataFinal}`;
        return requisicao.requisitarMetodoGET(urlConsultaComissoes);
    },
    async solicitarComissoes(comissoes) {
        return requisicao.requisitarMetodoPUT("/comissoes/solicitar-comissao", comissoes, false, false);
    },
    async enviarEmailSolicitacaoComissao(codigoSolicitacao) {
        const urlEnvioEmailSolicitacaoComissao = `/comissoes/enviar-email-solicitacao-pagamento/${codigoSolicitacao}`;
        return requisicao.requisitarMetodoPOST(urlEnvioEmailSolicitacaoComissao, "", true);
    }
};

const comissaoControle = {
    formatoMoeda: {
        pais: 'pt-BR',
        stilo: { style: 'currency', currency: 'BRL' }
    },
    comissoes: [],
    comissoesSelecionadas: [],
    marcaTodos: true,
    statusConsultaComissao: "LIBERADO",
    valorSelecionadoComissoes: 0,
    pegarPeridoDoMes: function () {
        let dataInicial = new Date();
        dataInicial.setMonth(0);
        dataInicial.setDate(1);
        const dataFinal = new Date(dataInicial.getFullYear(), 12, 0);
        const comissaoDataInicial = document.querySelector("#comissaoDataInicial");
        const comissaoDataFinal = document.querySelector("#comissaoDataFinal");
        filtro.dataInicial = dataInicial.toISOString().slice(0, 10);
        filtro.dataFinal = dataFinal.toISOString().slice(0, 10);
        comissaoDataInicial.value = dataInicial.toISOString().slice(0, 10);
        comissaoDataFinal.value = dataFinal.toISOString().slice(0, 10);
    },
    pegarDataSelecionada() {
        const comissaoDataInicial = document.querySelector("#comissaoDataInicial");
        const comissaoDataFinal = document.querySelector("#comissaoDataFinal");
        filtro.dataInicial = comissaoDataInicial.value;
        filtro.dataFinal = comissaoDataFinal.value;
    },
    selecionarRepresentante() {
        if (usuario.permissoes.representante) {
            filtro.representante = usuario.representante;
        }
    },
    somarTotalComissoes() {
        const totalComissoesParaSolicitar = this.comissoes
            .map(comissao => comissao.valor)
            .reduce((valorAnterio, valorCorrente) => valorAnterio + valorCorrente, 0);
        return totalComissoesParaSolicitar;
    },
    verificarSeExisteAlgumaComissaoSelecionada() {
        const inputsSelecionados = Array.from(document.querySelectorAll(".selecao"));
        const comissaoSelecionada = inputsSelecionados.some(elemento => elemento.checked);
        return comissaoSelecionada;
    },
    habilidarBotao() {
        const btSolicitarPagamento = document.getElementById("btSolicitarPagamento");
        const inputSelecionados = this.verificarSeExisteAlgumaComissaoSelecionada();
        btSolicitarPagamento.disabled = !inputSelecionados;
        if (!inputSelecionados) {
            document.getElementById("btMarcaTodos").textContent = "Marcar Todos";
            this.marcaTodos = true;
        }
    },
    selecionarTodos() {
        const inputsSelecionados = document.querySelectorAll(".selecao");
        const informacaoQuantidadeSelecionada = document.getElementById("quantidadeSelecionada");
        const valorAserSolicitado = document.getElementById("valorAserSolicitado");
        if (this.marcaTodos) {
            this.comissoesSelecionadas = this.comissoes.slice();
            inputsSelecionados.forEach(input => {
                if (input.getAttribute("data-informacao") == "semValor") {
                    return;
                }
                input.checked = true;
            });
            document.getElementById("btMarcaTodos").textContent = "Desmarcar Todos";
            this.marcaTodos = false;
            informacaoQuantidadeSelecionada.textContent = this.comissoesSelecionadas.length;
            const totalComissoesParaSolicitar = this.somarTotalComissoes();
            this.valorSelecionadoComissoes = totalComissoesParaSolicitar;
            valorAserSolicitado.textContent = totalComissoesParaSolicitar.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            this.habilidarBotao();
            return;
        }
        inputsSelecionados.forEach(input => input.checked = false);
        document.getElementById("btMarcaTodos").textContent = "Marcar Todos";
        this.marcaTodos = true;
        informacaoQuantidadeSelecionada.textContent = 0;
        valorAserSolicitado.textContent = "R$ 0,00";
        this.habilidarBotao();
        this.comissoesSelecionadas = [];
        this.valorSelecionadoComissoes = 0;
    },
    selecionar(input, indeceComissao) {
        const comissao = this.comissoes[indeceComissao];
        const valorAserSolicitado = document.getElementById("valorAserSolicitado");
        if (input.checked) {
            this.valorSelecionadoComissoes += comissao.valor;
            this.comissoesSelecionadas[indeceComissao] = comissao;
        } else {
            this.valorSelecionadoComissoes -= comissao.valor;
            const indeceNaComissaoSelecionada = this.comissoesSelecionadas.indexOf(comissao);
            this.comissoesSelecionadas.splice(indeceNaComissaoSelecionada, 1);
        }
        valorAserSolicitado.textContent = this.valorSelecionadoComissoes.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        this.habilidarBotao();
        const informacaoQuantidadeSelecionada = document.getElementById("quantidadeSelecionada");
        informacaoQuantidadeSelecionada.textContent = this.comissoesSelecionadas.length;
    },
    async listarTotais() {
        this.selecionarRepresentante();
        this.pegarDataSelecionada();
        const resposta = await (await comissaoService.listarTotais()).json();
        const totalLiberado = document.getElementById("totalLiberado");
        const totalPendente = document.getElementById("totalPendente");
        const totalPago = document.getElementById("totalPago");
        totalLiberado.textContent = resposta.totalLiberado.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        totalPendente.textContent = resposta.totalPendente.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        totalPago.textContent = resposta.totalPago.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
    },
    async listarComissoes() {
        this.selecionarRepresentante();
        this.pegarDataSelecionada();
        const listaComissoes = document.getElementById("listaComissoes");
        const resposta = await (await comissaoService.listarComissoes(this.statusConsultaComissao)).json();
        let linhas = "";
        this.comissoes = resposta;
        const liberarMarcacao = this.statusConsultaComissao != "LIBERADO";
        let indeceComissao = 0;
        for (const comissao of resposta) {
            const tipo = comissao.tipo == "INSTALACAO" ? "I" : "M";
            const badgeTipo = tipo == "I" ? "primary" : "secondary";
            const data = comissao.data ? comissao.data : "00/00/0000";
            const valor = comissao.valor.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            let badgeStatus = "warning";
            if (comissao.status == "LIBERADO") {
                badgeStatus = "success";
            } else if (comissao.status == "PAGO") {
                badgeStatus = "primary";
            }
            const desabilitarCheckBox = liberarMarcacao || comissao.valor == 0 ? "disabled" : "";
            let valorInstalacaoMensalidade = "Mensalidade: " + comissao.valorMensalidade.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            if (comissao.tipo == "INSTALACAO") {
                valorInstalacaoMensalidade = "Instalação: " + comissao.valorInstalacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            }
            const semValor = comissao.valor == 0 ? "semValor" : "comValor";
            linhas += `
            <div class="list-group-item flex-column align-items-start fade-form">
            <div class="d-flex justify-content-between">
                <span class="badge badge-${badgeStatus}"
                    style="font-size: 8pt;width: 20px; height: 20px; border-radius: 50px;">
                </span>
                <span class="badge badge-${badgeTipo} align-items-center"
                    style="font-size: 9pt;width: 20px; height: 20px; border-radius: 3px; margin-left: -50px;">
                    ${tipo}
                </span>
                <span style="font-size: 10pt;">Venc.: ${data}</span>
                <div class="checkbox">
                    <label style="font-size: 1em">
                        <input type="checkbox" class="selecao" data-informacao="${semValor}" ${desabilitarCheckBox} onclick="comissaoControle.selecionar(this,${indeceComissao})"/>
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
                <span>${comissao.razaoSocial}</span>
                <span class="ml-5" style="font-size: 20pt;">${valor}</span>
            </div>
            <div class="d-flex align-items-center justify-content-end">            
                <span class="ml-5" style="font-size: 10pt;">${valorInstalacaoMensalidade}</span>
            </div>
        </div>
            `;
            indeceComissao++;
        }
        listaComissoes.innerHTML = `
            <div class="list-group-item d-flex justify-content-between align-items-center">
                <span class="mt-4 ml-3"><span id="quantidadeSelecionada">0</span> Selecionados</span>
                <button type="button" id="btMarcaTodos" class="btn btn-purple shadow-sm mt-3 mr-3" onclick="comissaoControle.selecionarTodos()">Marcar Todos</button>
            </div>    `;
        listaComissoes.innerHTML += linhas;
        const existeComissaoMasSemValorParaSolicitacao = (this.comissoes.length == 1 && this.comissoes[0].valor == 0);
        const naoExisteComissoes = this.comissoes.length == 0;
        if (liberarMarcacao || naoExisteComissoes || existeComissaoMasSemValorParaSolicitacao) {
            document.getElementById("btMarcaTodos").setAttribute("disabled", "disabled");
            this.habilidarBotao();
        }
    },
    resetarValores() {
        this.valorSelecionadoComissoes = 0;
        this.marcaTodos = true;
        this.comissoes = [];
        this.comissoesSelecionadas = [];
        document.getElementById("valorAserSolicitado").textContent = "R$ 0,00";
    },
    async solicitarComissoes() {
        const solicitacoesComissao = this.comissoesSelecionadas.map(comissao => comissao.codigo);
        const resposta = await (await comissaoService.solicitarComissoes(solicitacoesComissao)).json();
        this.statusConsultaComissao = "LIBERADO";
        this.resetarValores();
        this.listarComissoes();
        this.listarTotais();
        $("#exampleModal").modal("hide");
        const comissao = resposta;
        comissaoService.enviarEmailSolicitacaoComissao(comissao.codigoSolicitacao);
    }
};

const btLiberado = document.getElementById("btLiberado");
btLiberado.addEventListener("click", evt => {
    comissaoControle.statusConsultaComissao = "LIBERADO";
    comissaoControle.resetarValores();
    comissaoControle.listarTotais();
    comissaoControle.listarComissoes();
});
const btPendente = document.getElementById("btPendente");
btPendente.addEventListener("click", evt => {
    comissaoControle.statusConsultaComissao = "PENDENTE";
    comissaoControle.resetarValores();
    comissaoControle.listarTotais();
    comissaoControle.listarComissoes();
});
const btPago = document.getElementById("btPago");
btPago.addEventListener("click", evt => {
    comissaoControle.statusConsultaComissao = "PAGO";
    comissaoControle.resetarValores();
    comissaoControle.listarTotais();
    comissaoControle.listarComissoes();
});
const btSolicitarPagamento = document.getElementById("btSolicitarPagamento");
btSolicitarPagamento.addEventListener("click", evt => {
    const totalComissoesParaSolicitar = comissaoControle.valorSelecionadoComissoes;
    document.getElementById("valorAserConfirmadoParaSolicitacao").textContent = totalComissoesParaSolicitar
        .toLocaleString(comissaoControle.formatoMoeda.pais, comissaoControle.formatoMoeda.stilo);
});
const btConfirmaComissao = document.getElementById("btConfirmaComissao");
btConfirmaComissao.addEventListener("click", evt => comissaoControle.solicitarComissoes());
comissaoControle.pegarPeridoDoMes();
const comissaoDataFinal = document.querySelector("#comissaoDataFinal");
const comissaoDataInicial = document.querySelector("#comissaoDataInicial");

comissaoDataInicial.addEventListener("change", evt => comissaoControle.listarTotais());
comissaoDataFinal.addEventListener("change", evt => comissaoControle.listarTotais());