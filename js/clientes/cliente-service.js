let clienteServico = {
    salvar: async function (cliente) {
        delete cliente.statusAtividade;
        return requisicao.requisitarMetodoPOST("/clientes", cliente, false);
    },
    alterar: async function (cliente, codigoCliente) {
        delete cliente.cnpj;
        delete cliente.dataCadastro;
        delete cliente.dataUltimoAcesso;
        delete cliente.dataUltimaValidacao;
        delete cliente.dataProximoVencimento;
        delete cliente.atualizouSistema;
        delete cliente.chaveSaiph;
        delete cliente.statusPedido;
        delete cliente.dataFimVigenciaContrato;
        return requisicao.requisitarMetodoPUT(`/clientes/${codigoCliente}`, cliente, false, false);
    },
    buscarCliente: async function (cnpj) {
        return requisicao.requisitarMetodoGET(`/clientes/${cnpj}`);
    },
    buscarCnpjReceita: function (cnpj, sucesso, beforeSend, error) {
        const urlReceita = `https://www.receitaws.com.br/v1/cnpj/${cnpj}`;
        $.ajax({
            type: 'GET'
            , url: urlReceita
            , dataType: 'jsonp'
            , beforeSend: beforeSend
            , success: sucesso
            , error: error
        });
    },
    async gerarSenha(cnpj) {
        return requisicao.requisitarMetodoPOST(`/clientes/${cnpj}/gerar-senha`, cliente, true);
    },
    async reenviarContrato(cliente) {
        return requisicao.requisitarMetodoPOST(`/pedidos/${cliente.cnpj}/reenviar-contrato`, cliente.novoEmail, false);
    },
    async listarSeguimentos(){
        return fetch("../../js/clientes/seguimentos-clientes.json");
    }
};