let campoCep = document.getElementById("cep");
campoCep.addEventListener("change", async event => {
    const promisse = await viaCep.buscarCep(campoCep.value.replace(/\D/g, ""));
    const data = await promisse.json();
    document.getElementById("logradouro").value = data.logradouro;
    document.getElementById("bairro").value = data.bairro;
    document.getElementById("cidade").value = data.localidade;
    document.getElementById("uf").value = data.uf;
    document.getElementById("numero").focus();
});