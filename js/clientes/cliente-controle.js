let cliente = { endereco: {} };

let clienteControle = {
    clienteEmAlteracao: false,
    codigoCliente: "",
    async salvar() {
        let alteracao = this.clienteEmAlteracao;
        try {
            this.desabilitarBotaoGravar();
            this.preencherClienteSalvar();
            let resposta = {};
            if (alteracao) {
                resposta = await (await clienteServico.alterar(cliente, this.codigoCliente)).text();
            } else {
                resposta = await (await clienteServico.salvar(cliente)).json();
                if (resposta.length >= 1 && resposta[0].mensagemDesenvolvedor) {
                    swal("Morpheus", resposta[0].titulo, "warning");
                    this.habilitarBotaoGravar();
                    return;
                }
                cliente = null;
                cliente = { endereco: {} };
                this.limparCampos();
            }
            if (!!resposta && resposta.length >= 1) {
                this.validarAntesDeSalvarCliente(resposta);
                this.habilitarBotaoGravar();
            } else {
                swal("Morpheus", "Salvo com sucesso!!", "success").then(() => {
                    document.getElementById("cnpj").focus();
                });
                this.habilitarBotaoGravar();
            }
        } catch (exception) {
            swal("Morpheus", exception.mensagemDesenvolvedor, "error");
            this.habilitarBotaoGravar();
        }
    },
    async buscarCliente(cnpj) {
        const conteudo = document.getElementById("conteudo");
        const spinner = document.getElementById("spinner");
        try {
            const resposta = await (await clienteServico.buscarCliente(cnpj)).json();
            conteudo.classList.remove("d-none");
            spinner.classList.add("d-none");
            return resposta;
        } catch (error) {
            swal("Não foi possivel consultar a pessoa", "por favor verifique sua conexão com a internet", "error").then(() => {
                window.location.href = "clientes-lista.html";
            });
        }
    },
    async gerarSenha() {
        const resposta = await (await clienteServico.gerarSenha(this.codigoCliente)).json();
        cliente = resposta;
        return resposta;
    },
    async visulizarContrato() {
        return fetch(`${servidor.URL_API}/pedidos/${this.codigoCliente}/visualizar-contrato-cliente`, {
            headers: {
                "Authorization": usuario.autorizacao
            }
        });
    },
    async reenviarEmail(emailInformado) {
        if (!emailInformado) {
            emailInformado = cliente.email;
        }
        const clienteEmail = { cnpj: this.codigoCliente, novoEmail: { email: emailInformado } };
        clienteServico.reenviarContrato(clienteEmail);
    },
    async listarSeguimentos() {
        const selectSeguimentos = document.querySelector("#seguimento");
        const seguimentos = await (await clienteServico.listarSeguimentos()).json();
        let linhas = "";
        for (const seguimento of seguimentos) {
            linhas += `<option value="${seguimento.toUpperCase()}">${seguimento.toUpperCase()}</option>`;
        }
        selectSeguimentos.innerHTML = linhas;
    },
    preencherCliente(clienteEncontrado) {
        cliente = clienteEncontrado;
        document.getElementById("cnpj").value = clienteEncontrado.cnpj;
        document.getElementById("inscricaoEstadual").value = clienteEncontrado.inscricaoEstadual;
        document.getElementById("razaoSocial").value = clienteEncontrado.razaoSocial;
        document.getElementById("nomeFantasia").value = clienteEncontrado.nomeFantasia;
        document.getElementById("regimeTributario").value = clienteEncontrado.regimeTributario;
        document.getElementById("logradouro").value = clienteEncontrado.endereco.logradouro;
        document.getElementById("numero").value = clienteEncontrado.endereco.numero;
        document.getElementById("bairro").value = clienteEncontrado.endereco.bairro;
        document.getElementById("cidade").value = clienteEncontrado.endereco.cidade;
        document.getElementById("uf").value = clienteEncontrado.endereco.estado;
        document.getElementById("complemento").value = clienteEncontrado.endereco.complemento;
        document.getElementById("cep").value = clienteEncontrado.endereco.cep;
        if (clienteEncontrado.seTef == "S") {
            document.getElementById("seTef").setAttribute("checked", "checked");
        }
        document.getElementById("obs").value = clienteEncontrado.obs;
        if (clienteEncontrado.contingencia == "S") {
            document.getElementById("contingencia").setAttribute("checked", "checked");
        }
        if (clienteEncontrado.onSaiph == "S") {
            document.getElementById("onSaiph").setAttribute("checked", "checked");
        }
        document.getElementById("diaVencimento").value = clienteEncontrado.diaVencimento;
        document.getElementById("prazoMaximo").value = clienteEncontrado.prazoMaximo;
        document.getElementById("statusPagamento").value = clienteEncontrado.statusPagamento;
        if (clienteEncontrado.codigoGrupoEmpresa != null) {
            document.getElementById("grupoEmpresa").value = clienteEncontrado.codigoGrupoEmpresa.codigo;
        }
        document.getElementById("nomeContato").value = clienteEncontrado.nomeContato;
        document.getElementById("telefone").value = clienteEncontrado.telefone;
        document.getElementById("celular").value = clienteEncontrado.celular;
        document.getElementById("whatsapp").value = clienteEncontrado.whatsapp;
        document.getElementById("email").value = clienteEncontrado.email;
        document.getElementById("seguimento").value = clienteEncontrado.seguimento;
        document.getElementById("cpfPessoaResponsavel").value = clienteEncontrado.cpfPessoaResponsavel;
        document.getElementById("nomePessoaResponsavel").value = clienteEncontrado.nomePessoaResponsavel;
        document.getElementById("dataVigenciaContrato").textContent = clienteEncontrado.dataAceite;
        document.getElementById("dataFimVigenciaContrato").textContent = clienteEncontrado.dataFimVigenciaContrato;
        if (clienteEncontrado.statusAtividade == "I") {
            document.getElementById("statusAtividade").setAttribute("checked", "checked");
        }
        if (cliente.dataProximoVencimento) {
            document.getElementById("dataProximoVencimento").textContent = cliente.dataProximoVencimento;
        }
        if (cliente.dataUltimaValidacao) {
            document.getElementById("dataUltimaValidacao").textContent = cliente.dataUltimaValidacao;
        }
        if (cliente.dataUltimoAcesso) {
            document.getElementById("dataUltimoAcesso").textContent = cliente.dataUltimoAcesso;
        }
        if (cliente.dataCadastro) {
            document.getElementById("dataCadastro").textContent = cliente.dataCadastro;
        }
        document.getElementById("chaveSaiph").textContent = cliente.chaveSaiph;
    },
    preencherClienteSalvar() {
        cliente.cnpj = document.getElementById("cnpj").value.replace(/\D/g, "");
        cliente.inscricaoEstadual = document.getElementById("inscricaoEstadual").value;
        cliente.razaoSocial = document.getElementById("razaoSocial").value;
        cliente.nomeFantasia = document.getElementById("nomeFantasia").value;
        cliente.regimeTributario = document.getElementById("regimeTributario").value;
        cliente.endereco.logradouro = document.getElementById("logradouro").value;
        cliente.endereco.numero = document.getElementById("numero").value;
        cliente.endereco.bairro = document.getElementById("bairro").value;
        cliente.endereco.cidade = document.getElementById("cidade").value;
        cliente.endereco.estado = document.getElementById("uf").value;
        cliente.endereco.complemento = document.getElementById("complemento").value;
        cliente.endereco.cep = document.getElementById("cep").value.replace(/\D/g, "");
        cliente.seTef = document.getElementById("seTef").checked ? "S" : "N";
        cliente.obs = document.getElementById("obs").value;
        cliente.contingencia = document.getElementById("contingencia").checked ? "S" : "N";
        cliente.onSaiph = document.getElementById("onSaiph").checked ? "S" : "N";
        cliente.statusAtividade = document.getElementById("statusAtividade").checked ? "I" : "A";
        cliente.diaVencimento = document.getElementById("diaVencimento").value;
        cliente.prazoMaximo = document.getElementById("prazoMaximo").value;
        cliente.statusPagamento = document.getElementById("statusPagamento").value;
        cliente.codigoRepresentante = usuario.representante;
        cliente.codigoGrupoEmpresa = document.getElementById("grupoEmpresa").value;
        cliente.nomeContato = document.getElementById("nomeContato").value;
        cliente.telefone = document.getElementById("telefone").value.replace(/\D/g, "");
        cliente.celular = document.getElementById("celular").value.replace(/\D/g, "");
        cliente.whatsapp = document.getElementById("whatsapp").value.replace(/\D/g, "");
        cliente.email = document.getElementById("email").value;
        cliente.seguimento = document.getElementById("seguimento").value;
        cliente.cpfPessoaResponsavel = document.getElementById("cpfPessoaResponsavel").value.replace(/\D/g, "");
        cliente.nomePessoaResponsavel = document.getElementById("nomePessoaResponsavel").value;
    },
    preencherClienteReceita(data) {
        document.getElementById("razaoSocial").value = data.nome;
        document.getElementById("nomeFantasia").value = data.fantasia;
        document.getElementById("logradouro").value = data.logradouro;
        document.getElementById("numero").value = data.numero;
        document.getElementById("bairro").value = data.bairro;
        document.getElementById("cidade").value = data.municipio;
        document.getElementById("uf").value = data.uf;
        document.getElementById("complemento").value = data.complemento;
        document.getElementById("cep").value = data.cep.replace(/\D/g, "");
        document.getElementById("celular").value = data.telefone.replace(/\D/g, "");
        document.getElementById("email").value = data.email;
    },
    validarAntesDeSalvarCliente(resposta) {
        let mensagemErro = "";
        for (const erro of resposta) {
            mensagemErro += erro.titulo + " \n";
        }
        swal("Morpheus", mensagemErro, "warning");
    },
    desabilitarBotaoGravar() {
        document.getElementById("buttonGravarCliente").setAttribute("disabled", "disabled");
        document.getElementById("loading").classList.remove("d-none");
    },
    habilitarBotaoGravar() {
        document.getElementById("buttonGravarCliente").removeAttribute("disabled");
        document.getElementById("loading").classList.add("d-none");
    },
    limparCampos() {
        const inputs = document.querySelectorAll("input");
        inputs.forEach(elemento => {
            elemento.value = "";
        });
        const checks = document.querySelectorAll("input[type='checkbox']");
        checks.forEach(elemento => {
            elemento.removeAttribute("checked");
        });
    }
};