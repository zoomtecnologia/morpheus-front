function consultarCnpj(input) {
    const cnpjConteudo = input.value.replace(/\D/g, "");
    if (cnpjConteudo.trim() == "") {
        return;
    }
    if (cnpjConteudo.length < 14) {
        swal("Morpheus", "Você não digitou um cnpj.", "warning").then(success => {
            input.focus();
            input.select();
        });
        return;
    }
    return clienteControle.buscarCliente(cnpjConteudo);
}