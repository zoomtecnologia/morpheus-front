const filtroComissao = {
    status: "SOLICITADO",
    codigoRepresetante: "",
    nomeRepresentante: ""
};

const pagamentoComissoesServico = {
    async listarSoclicitacaoPagamento() {
        const urlSolicitacaoDePagamento = `/comissoes/solicitacao-pagamentos?representante=${filtroComissao.codigoRepresetante}&status=${filtroComissao.status}&nomeRepresentante=${filtroComissao.nomeRepresentante}`;
        return requisicao.requisitarMetodoGET(urlSolicitacaoDePagamento);
    },
    async realizarPagamentoSolicitacao(codigoSolicitacao) {
        return requisicao.requisitarMetodoPUT(`/comissoes/pagamento-solicitacao/${codigoSolicitacao}`, "", false, false);
    },
    async listarComissoesPorSolicitacao(codigoSolicitacao) {
        const urlComissao = `/comissoes/comissoes-status?periodo=true&data=false&codigoSolicitacao=${codigoSolicitacao}`;
        return requisicao.requisitarMetodoGET(urlComissao);
    },
    async enviarEmailSolicitacaoComissao(codigoSolicitacao) {
        const urlEnvioEmailSolicitacaoComissao = `/comissoes/enviar-email-solicitacao-pagamento/${codigoSolicitacao}`;
        return requisicao.requisitarMetodoPOST(urlEnvioEmailSolicitacaoComissao, "", true);
    }
};

const pagamentoComissoesControle = {
    formatoMoeda: {
        pais: 'pt-BR',
        stilo: { style: 'currency', currency: 'BRL' }
    },
    solicitacoes: [],
    solicitacaoSelecionada: "",
    async realizarPagamento() {
        const resposta = await (await pagamentoComissoesServico.realizarPagamentoSolicitacao(this.solicitacaoSelecionada)).text();
        this.listarSoclicitacaoPagamento();
        $("#modalConfirmacao").modal("hide");
        pagamentoComissoesServico.enviarEmailSolicitacaoComissao(this.solicitacaoSelecionada);
        this.solicitacaoSelecionada = "";
    },
    async listarSoclicitacaoPagamento() {
        this.selecionarRepresentante();
        const listaSolicitacoesPagamento = document.getElementById("listaSolicitacoesPagamento");
        const resposta = await (await pagamentoComissoesServico.listarSoclicitacaoPagamento()).json();
        let linhas = "";
        let indece = 0;
        this.solicitacoes = resposta;
        for (const solicitacao of resposta) {
            const valorSolicitacao = solicitacao.totalSolicitacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            const dataPagamento = solicitacao.dataPagamento ? solicitacao.dataPagamento : "";
            const dataSolicitacao = solicitacao.dataSolicitacao ? solicitacao.dataSolicitacao : "";
            const desabilitarPagamento = usuario.permissoes.representante || filtroComissao.status == "PAGO" ? "d-none" : "";
            linhas += `
            <li class="list-group-item shadow mb-2 flex-column align-items-start fade-form">
            <div class="d-flex w-100 justify-content-start">
                <p class="mr-3" style="font-size: 10pt;">Solicitação: ${dataSolicitacao}</p>
                <p style="font-size: 10pt;">Paga: ${dataPagamento}</p>
            </div>
            <div class="d-flex w-100 justify-content-between">
                <h6 class="mb-1 font-weight-bold" style="font-size: 10pt;">${solicitacao.codigoSolicitacao}</h6>
                <span class="font-weight-bold" style="font-size: 25pt;">${valorSolicitacao}</span>
            </div>
            <p class="mb-1 font-weight-bold" style="font-size: 9pt;text-align: right;">
                ${solicitacao.nomeRepresentante}
            </p>
            <hr>
            <div class="d-flex w-100 justify-content-end">
                <a class="btn btn-grid-pg m-2 ${desabilitarPagamento}" href="pagamento-pix.html?comissao=${solicitacao.codigoSolicitacao}">
                    <i class="fas fa-dollar-sign"></i>
                </a>
                <button type="button" class="btn btn-grid-pg m-2 ${desabilitarPagamento}" data-toggle="modal" data-target="#modalConfirmacao" onclick="pagamentoComissoesControle.selecionarSolocitacao('${indece}')">
                    <i class="fab fa-product-hunt"></i>
                </button>
                <button class="btn btn-grid-pg m-2" data-toggle="modal" data-target="#exampleModal" onclick="pagamentoComissoesControle.listarComissoesPorSolicitacao(${indece})">
                    <i class="fas fa-list"></i>
                </button>
            </div>
        </li>`;
            indece++;
        }
        listaSolicitacoesPagamento.innerHTML = linhas;
    },
    async listarComissoesPorSolicitacao(indece) {
        const solicitacao = this.solicitacoes[indece];
        const resposta = await (await pagamentoComissoesServico.listarComissoesPorSolicitacao(solicitacao.codigoSolicitacao)).json();
        document.getElementById("codigoSolicitacao").textContent = solicitacao.codigoSolicitacao;
        document.getElementById("valorDaSolicitacao").textContent = solicitacao.totalSolicitacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        const listaComissoes = document.getElementById("listaComissoes");
        let linhas = "";
        for (const comissao of resposta) {
            const data = comissao.data ? comissao.data : "";
            const valorComissao = comissao.valor.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            let valorInstalacaoMensalidade = "Mensalidade: " + comissao.valorMensalidade.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            if (comissao.tipo == "INSTALACAO") {
                valorInstalacaoMensalidade = "Instalação: " + comissao.valorInstalacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
            }
            linhas += `
            <li class="list-group-item flex-column align-items-start">
            <p class="mb-1 font-weight-bold" style="font-size: 9pt;text-align: left;">
                ${comissao.razaoSocial}
            </p>
            <div class="d-flex w-100 justify-content-start">
                <span class="mr-3" style="font-size: 10pt;">${comissao.tipo}</span>
                <span style="font-size: 10pt;">Parcela ${data}</span>
            </div>
            <div class="d-flex w-100 justify-content-end">
                <span class="font-weight-bold" style="font-size: 12pt;">Comissão: ${valorComissao}</span>                
            </div>
            <div class="d-flex w-100 justify-content-end">   
                <small>${valorInstalacaoMensalidade}</small>
            </div>
            </li>`;
        }
        listaComissoes.innerHTML = linhas;
    },
    selecionarRepresentante() {
        if (usuario.permissoes.representante) {
            filtroComissao.codigoRepresetante = usuario.representante;
        }
    },
    selecionarSolocitacao(indece) {
        const solicitacao = this.solicitacoes[indece];
        this.solicitacaoSelecionada = solicitacao.codigoSolicitacao;
        const valorAserConfirmadoParaPagamento = document.getElementById("valorAserConfirmadoParaPagamento");
        const informacaoRepresentante = document.getElementById("informacaoRepresentante");
        const modalSolicitacao = document.getElementById("modalSolicitacao");
        valorAserConfirmadoParaPagamento.textContent = solicitacao.totalSolicitacao.toLocaleString(this.formatoMoeda.pais, this.formatoMoeda.stilo);
        informacaoRepresentante.textContent = solicitacao.nomeRepresentante;
        modalSolicitacao.textContent = solicitacao.codigoSolicitacao;
    }
};

const buttonPesquisar = document.getElementById("buttonPesquisar");
const pesquisar = document.getElementById("pesquisar");
pesquisar.addEventListener("keypress", evt => {
    if (evt.keyCode == 13) {
        buttonPesquisar.click();
    }
});
const radioSolicitada = document.getElementById("radioSolicitada");
const radioPaga = document.getElementById("radioPaga");

radioSolicitada.addEventListener("click", evt => buttonPesquisar.click());
radioPaga.addEventListener("click", evt => buttonPesquisar.click());

buttonPesquisar.addEventListener("click", evt => {
    const conteudoDaPesquisa = pesquisar.value.trim();
    const status = radioSolicitada.checked ? "SOLICITADO" : "PAGO";
    filtroComissao.status = status;
    filtroComissao.nomeRepresentante = conteudoDaPesquisa;
    if (!usuario.permissoes.representante) {
        filtroComissao.codigoRepresetante = "";
    }
    pagamentoComissoesControle.listarSoclicitacaoPagamento();
    pesquisar.focus();
});
const btConfirmaComissao = document.getElementById("btConfirmaComissao");
btConfirmaComissao.addEventListener("click", evt => pagamentoComissoesControle.realizarPagamento());