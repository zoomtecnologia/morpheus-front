const lista = document.querySelector("#lista");
const totalElementos = document.querySelector("#totalElementos");
const loading = document.querySelector("#loading");

const grupoEmpresaServico = {
    async listarEmpresas() {
        return requisicao.requisitarMetodoGET("/grupo-empresas");
    }
};

const grupoEmpresaControle = {
    async listarEmpresas() {
        this.limparConsulta();
        let gruposEmpresas = [];
        try {
            gruposEmpresas = await (await grupoEmpresaServico.listarEmpresas()).json();
        } catch (error) {
            swal("Não foi possivel carregar as informações", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        totalElementos.textContent = gruposEmpresas.length == 1 ? `grupo ${gruposEmpresas.length}` : `grupos ${gruposEmpresas.length}`;
        let linhas = "";
        for (const grupoEmpresa of gruposEmpresas) {
            linhas += `
            <li class="list-group-item shadow mb-2 flex-column align-items-start">
            <div class="d-flex w-100 justify-content-start">
                <h6 class="mb-1 font-weight-bold" style="font-size: 14pt;">
                    ${grupoEmpresa.nome}
                </h6>
            </div>
            <hr>
            <div class="d-flex w-100 justify-content-end">
                <button class="btn btn-grid-pg m-2" onclick="location.href='grupos-empresa.html?grupo=${grupoEmpresa.codigo}'">
                    <span id="" class="spinner-grow spinner-grow-sm d-none"
                        role="status" aria-hidden="true"></span>
                    <i class="fas fa-edit"></i>
                    </button>
                </div>
            </li>`;
        }
        loading.classList.add("d-none");
        lista.innerHTML = linhas;
    },
    limparConsulta() {
        lista.innerHTML = "";
        loading.classList.remove("d-none");
        totalElementos.textContent = "0";
    }
};