const nomeGrupoEmpresa = document.querySelector("#nome");
const formulario = document.querySelector("#formulario");
const loadingSalvar = document.querySelector("#loading-salvar");

const grupoEmpresaServico = {
    async buscarGrupoEmpresa(codigo) {
        return requisicao.requisitarMetodoGET(`/grupo-empresas/${codigo}`);
    },
    async salvarGrupoEmpresa(grupoEmpresa) {
        return requisicao.requisitarMetodoPOST("/grupo-empresas", grupoEmpresa, false);
    },
    async alterarGrupoEmpresa(grupoEmpresa, codigo) {
        return requisicao.requisitarMetodoPUT(`/grupo-empresas/${codigo}`, grupoEmpresa, false, false);
    }
};

const grupoEmpresaControle = {
    codigoGrupoEmpresa: 0,
    grupoEmpresa: {},
    alteracao: false,
    async buscarGrupoEmpresa() {
        const resposta = await (await grupoEmpresaServico.buscarGrupoEmpresa(this.codigoGrupoEmpresa)).json();
        nomeGrupoEmpresa.value = resposta.nome;
    },
    async salvarGrupoEmpresa() {
        try {
            this.habilitarLoadingNoSalvar();
            const resposta = await (await grupoEmpresaServico.salvarGrupoEmpresa(this.grupoEmpresa)).json();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            swal("Salvo com sucesso!!", "Morpheus", "success");
            this.limparCampos();
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        this.desabilitarLoadingNoSalvar();
    },
    async alterarGrupoEmpresa() {
        try {
            this.habilitarLoadingNoSalvar();
            const resposta = await (await grupoEmpresaServico.alterarGrupoEmpresa(this.grupoEmpresa, this.codigoGrupoEmpresa)).text();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            swal("Salvo com sucesso!!", "Morpheus", "success");
        } catch (error) {
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        this.desabilitarLoadingNoSalvar();
    },
    limparCampos() {
        nomeGrupoEmpresa.value = "";
        nomeGrupoEmpresa.focus();
    },
    mostrarInformacoes() {
        const parametro = window.location.search.slice(1);
        const grupoID = parametro.replace(/grupo=/, "");
        if (!grupoID) {
            grupoEmpresaControle.alteracao = false;
            return;
        }
        grupoEmpresaControle.codigoGrupoEmpresa = grupoID;
        grupoEmpresaControle.alteracao = true;
        grupoEmpresaControle.buscarGrupoEmpresa();
    },
    habilitarLoadingNoSalvar() {
        loadingSalvar.classList.remove("d-none");
        loadingSalvar.disabled = true;
    },
    desabilitarLoadingNoSalvar() {
        loadingSalvar.classList.add("d-none");
        loadingSalvar.disabled = false;
    }
};

formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    grupoEmpresaControle.grupoEmpresa.nome = nomeGrupoEmpresa.value;
    if (grupoEmpresaControle.alteracao) {
        grupoEmpresaControle.alterarGrupoEmpresa();
        return;
    }
    grupoEmpresaControle.salvarGrupoEmpresa();
});

