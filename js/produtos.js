const produtoService = {
    async salvarItem(item) {
        const urlItem = `/itens`;
        return requisicao.requisitarMetodoPOST(urlItem, item, false);
    },
    async alterarItem(item, codigoItem) {
        delete item.codigo;
        const urlItem = `/itens/${codigoItem}`;
        return requisicao.requisitarMetodoPUT(urlItem, item, false);
    },
    async buscarItem(codigoItem) {
        return requisicao.requisitarMetodoGET(`/itens/${codigoItem}`);
    }
};

const produtoControle = {
    item: {},
    codigoItem: 0,
    produtoEmAlteracao: false,
    async salvarItem() {
        this.preencherItemParaSalvar();
        let resposta = {};
        if (this.produtoEmAlteracao) {
            resposta = await (await produtoService.alterarItem(this.item, this.codigoItem)).text();
        } else {
            resposta = await (await produtoService.salvarItem(this.item)).json();
        }
        if (resposta.mensagemDesenvolvedor) {
            swal("AVISO", resposta.titulo, "warning");
            return;
        }
        if (!this.produtoEmAlteracao) {
            this.limparCampos();
        }
        swal("Produto Salvo", "", "success");
    },
    async buscarItem(item) {
        const resposta = await (await produtoService.buscarItem(item)).json();
        this.item = resposta;
        this.preencherItemNaTela();
    },
    preencherItemParaSalvar() {
        const descricao = document.getElementById("descricao").value;
        const valor = document.getElementById("valor").value;
        const valorMinimo = document.getElementById("valorMinimo").value;
        const inativo = document.getElementById("inativo");
        const unico = document.getElementById("unico");
        const opcional = document.getElementById("opcional");
        this.item.descricao = descricao;
        this.item.valor = valor;
        this.item.status = inativo.checked ? "I" : "A";
        this.item.itemUnico = unico.checked ? true : false;
        this.item.valorMinimo = valorMinimo;
        this.item.opcional = opcional ? true : false;
    },
    preencherItemNaTela() {
        const descricao = document.getElementById("descricao");
        const valor = document.getElementById("valor");
        const valorMinimo = document.getElementById("valorMinimo");
        const inativo = document.getElementById("inativo");
        const unico = document.getElementById("unico");
        const opcional = document.getElementById("opcional");
        descricao.value = this.item.descricao;
        valor.value = this.item.valor;
        inativo.checked = this.item.status == "I";
        unico.checked = this.item.itemUnico;
        valorMinimo.value = this.item.valorMinimo;
        opcional.checked = this.item.opcional;
    },
    limparCampos() {
        document.getElementById("descricao").value = "";
        document.getElementById("valor").value = "";
        document.getElementById("valorMinimo").value = "";
        document.getElementById("inativo").checked = false;
        document.getElementById("unico").checked = false;
        document.getElementById("opcional").checked = false;
        this.item = null;
        this.item = {};
        this.produtoEmAlteracao = false;
    },
    iniciarEventos() {
        const formularioProduto = document.getElementById("formularioProduto");
        formularioProduto.addEventListener("submit", evt => {
            evt.preventDefault();
            this.salvarItem();
        });
    },
    mostrarInformacao() {
        const parametro = window.location.search.slice(1);
        const codigoItem = parametro.replace(/item=/, "");
        if (!codigoItem) {
            this.produtoEmAlteracao = false;
            return;
        }
        this.codigoItem = codigoItem;
        this.produtoEmAlteracao = true;
        this.buscarItem(codigoItem);
    }
};