const scrollPaginacao = {
    getDocumentHeight: function () {
        const body = document.body;
        const html = document.documentElement;
        return Math.max(
            body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight
        );
    },
    getScrollTop: function () {
        return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    },
    getScrollFinal: function () {
        return (scrollPaginacao.getDocumentHeight() - window.innerHeight);
    },
    scrollPagination: function (acionarPaginacao) {
        let scrollFinal = this.getScrollFinal();
        if (scrollPaginacao.getScrollTop() >= (scrollFinal - 30)) {
            acionarPaginacao();
        }
    }
};