const loginServico = {
    async realizarLogin(usuario) {
        return fetch(`${servidor.URL_API}/usuarios/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
            body: JSON.stringify(usuario)
        });
    }
};

const loginControle = {
    usuario: {},
    async realizarLogin() {
        try {
            const resposta = await (await loginServico.realizarLogin(this.usuario)).json();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return;
            }
            if (resposta.status == "I") {
                swal(resposta.titulo, "Morpheus", "warning");
                return;
            }
            const usuarioLogado = this.preencherUsuarioLogado(resposta);
            const basicAuthorization = btoa(this.usuario.email + ":" + this.usuario.senha);
            usuarioLogado.autorizacao = `Basic ${basicAuthorization}`;
            localStorage.setItem("usuario", JSON.stringify(usuarioLogado));
            location.href = "../visao/clientes/clientes-lista.html";
        } catch (error) {
            if (error == "TypeError: Failed to fetch") {
                swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
            }
        }
    },
    preencherUsuarioLogado(resposta) {
        const usuarioLogado = {
            nome: resposta.nome,
            nomeRepresentante: resposta.representante.nome,
            representante: resposta.representante.documento,
            tipoComissao: 1,
            permissoes: {
                representante: resposta.permissao.representante,
                nome: resposta.permissao.nome,
                aplicacoes: converterAplicao(resposta.permissao.aplicacoes)
            },
            status: resposta.status,            
            NOME_APLICACAO: ""
        };
        function converterAplicao(aplicacoes) {
            const aplicacoesCustomizadas = aplicacoes.map(aplicacao => {
                return {
                    acessar: aplicacao.acessar,
                    alterar: aplicacao.alterar,
                    excluir: aplicacao.excluir,
                    incluir: aplicacao.incluir,
                    nome: aplicacao.permissaoAplicacao.aplicacao.nome,
                };
            });
            return aplicacoesCustomizadas;
        }
        return usuarioLogado;
    }
};

const formulario = document.querySelector("#formulario");
const loginEmail = document.querySelector("#loginEmail");
const loginSenha = document.querySelector("#loginSenha");
const loading = document.querySelector("#loading");
const buttonLogin = document.querySelector("#buttonLogin");

formulario.onsubmit = function (evento) {
    evento.preventDefault();
    loginControle.usuario.email = loginEmail.value;
    loginControle.usuario.senha = loginSenha.value;
    loading.classList.remove("d-none");
    buttonLogin.disabled = true;
    loginControle.realizarLogin().then(() => {
        loading.classList.add("d-none");
        buttonLogin.disabled = false;
    });
};