const usuarioLogado = JSON.parse(localStorage.getItem("usuario"));
if (!usuarioLogado) {
    location.href = "../../index.html";
}
const aplicacoes = usuario.permissoes.aplicacoes;
const aplicacao = aplicacoes.filter(aplicacao => aplicacao.nome == usuario.NOME_APLICACAO)[0];
if (!aplicacao.acessar) {
    location.href = "../pagina-negada/pagina-negada.html";
}