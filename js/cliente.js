const formularioCliente = document.querySelector("#formularioCliente");
const cnpj = document.querySelector("#cnpj");
const buttonGerarSenha = document.querySelector("#buttonGerarSenha");
const buttonVisualizarContrato = document.querySelector("#buttonVisualizarContrato");
const emailNovo = document.querySelector("#emailNovo");
const radioEmailExistente = document.querySelector("#radioEmailExistente");
const radioEmailNovo = document.querySelector("#radioEmailNovo");
const buttonEnviarEmail = document.querySelector("#buttonEnviarEmail");
const mostrarContrato = document.querySelector("#mostrarContrato");

formularioCliente.addEventListener("submit", event => {
    event.preventDefault();
    clienteControle.salvar();
});

function listarGruposDeEmpresa() {
    const gruposDeEmpresa = grupoEmpresaControle.listarGrupoEmpresa();
    gruposDeEmpresa.then(resposta => {
        const comboGrupoDeEmpresa = document.querySelector("#grupoEmpresa");
        for (const grupoEmpresa of resposta) {
            comboGrupoDeEmpresa.innerHTML += `
               <option value="${grupoEmpresa.codigo}">${grupoEmpresa.nome}</option>       
            `;
        }
    });
}

cnpj.addEventListener("change", evt => {
    consultarCnpj(cnpj).then(resposta => {
        const mensagem = `Cliente já Cadastrado ${resposta.cnpj} - ${resposta.razaoSocial}`;
        if (resposta.mensagemDesenvolvedor) {
            clienteServico.buscarCnpjReceita(cnpj.value.replace(/\D/g, ""), data => {
                clienteControle.preencherClienteReceita(data);
            });
            return;
        }
        swal("Morpheus", mensagem, "info").then(success => {
            cnpj.focus();
            cnpj.select();
        });
    }).catch(erro => swal("Morpheus", erro, "error"));
});
buttonGerarSenha.addEventListener("click", evt => {
    if (!clienteControle.clienteEmAlteracao) {
        return;
    }
    clienteControle.gerarSenha().then(cliente => {
        swal("Gerado a chave", cliente.chaveSaiph, "info");
        document.querySelector("#dataProximoVencimento").textContent = cliente.dataProximoVencimento;
        document.querySelector("#dataUltimaValidacao").textContent = cliente.dataUltimaValidacao;
        if (cliente.dataUltimoAcesso) {
            document.querySelector("#dataUltimoAcesso").textContent = cliente.dataUltimoAcesso;
        }
        document.querySelector("#chaveSaiph").textContent = cliente.chaveSaiph;
    });
});
buttonVisualizarContrato.addEventListener("click", async evt => {
    const loadingVisualizarContrato = document.querySelector("#loadingVisualizarContrato");
    loadingVisualizarContrato.classList.remove("d-none");
    buttonVisualizarContrato.disabled = true;
    const dados = await (await clienteControle.visulizarContrato()).arrayBuffer();
    loadingVisualizarContrato.classList.add("d-none");
    buttonVisualizarContrato.disabled = false;
    const blob = new Blob([dados], { type: 'application/pdf' });
    const url = URL.createObjectURL(blob);
    window.open(url, "_blank");
});

radioEmailExistente.addEventListener("click", evt => {
    emailNovo.classList.add("d-none");
});
radioEmailNovo.addEventListener("click", evt => {
    emailNovo.classList.remove("d-none");
    emailNovo.focus();
});
buttonEnviarEmail.addEventListener("click", evt => {
    if (radioEmailNovo.checked && !emailNovo.value) {
        swal("Informe um email", "Morpheus", "warning").then(() => emailNovo.focus());
        return;
    } +
        clienteControle.reenviarEmail(emailNovo.value);
    swal("E-mail enviado", "Morpheus", "success");
});
mostrarContrato.addEventListener("click", evt => {
    radioEmailExistente.checked = true;
    emailNovo.value = "";
    emailNovo.classList.add("d-none");
});
window.onload = function () {
    $('.cnpj').mask('00.000.000/0000-00');
    $('.cpf').mask('000.000.000-00');
    $('.celular').mask('(00)00000-0000');
    $('.cep').mask('00000-000');
    // $('#seguimento').select2();
    clienteControle.limparCampos();
    listarGruposDeEmpresa();
    clienteControle.listarSeguimentos();
    const cardEspecial = document.querySelector("#card-especial");
    if (!usuario.permissoes.representante) {
        cardEspecial.classList.remove("d-none");
        buttonGerarSenha.classList.remove("d-none");
    } else {
        cardEspecial.classList.add("d-none");
        document.querySelector("#mostrarContrato").classList.add("d-none");
    }
    const parametro = window.location.search.slice(1);
    const clienteId = parametro.replace(/cnpj=/, "");
    if (!clienteId) {
        clienteControle.clienteEmAlteracao = false;
        buttonGerarSenha.disabled = true;
        return;
    }
    buttonGerarSenha.disabled = false;
    clienteControle.codigoCliente = clienteId;
    clienteControle.clienteEmAlteracao = true;
    const conteudo = document.querySelector("#conteudo");
    conteudo.classList.add("d-none");
    const spinner = document.querySelector("#spinner");
    spinner.classList.remove("d-none");
    clienteControle.buscarCliente(clienteId).then(resposta => {
        clienteControle.preencherCliente(resposta);
        if (!usuario.permissoes.representante && resposta.aceiteContrato) {
            document.querySelector("#mostrarContrato").classList.remove("d-none");
        }
    });
};