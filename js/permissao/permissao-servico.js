const permissaoServico = {
    async listarPermissoes(resumida) {
        if (resumida) {
            return requisicao.requisitarMetodoGET("/permissoes?resumo=true");
        }
        return requisicao.requisitarMetodoGET("/permissoes");
    },
    async buscarPermissaoPor(codigo) {
        return requisicao.requisitarMetodoGET(`/permissoes/${codigo}`);
    },
    async salvar(permissao) {
        return requisicao.requisitarMetodoPOST("/permissoes", permissao, false);
    },
    async alterar(permissao, codigo) {
        return requisicao.requisitarMetodoPUT(`/permissoes/${codigo}`, permissao, false, false);
    },
    async listarAplicacoes() {
        return requisicao.requisitarMetodoGET(`/aplicacoes`);
    },
    async listarPermissaoAplicacoes(codigo) {
        return requisicao.requisitarMetodoGET(`/permissao-aplicaoes/${codigo}/permissao`);
    },
    async salvarPermissaoAplicacao(permissaoAplicacao) {
        return requisicao.requisitarMetodoPOST("/permissao-aplicaoes", permissaoAplicacao, false);
    }
};