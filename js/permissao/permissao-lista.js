const lista = document.querySelector("#lista");
const loading = document.querySelector("#loading");

const permissaoControle = {
    async listarPermissoes() {
        loading.classList.remove("d-none");
        let resposta = [];
        try {
            resposta = await (await permissaoServico.listarPermissoes(true)).json();
        } catch (error) {
            swal("Não foi possivel carregar as informações", "sistema indisponível! tente novamente mais tarde.", "error");
        }
        loading.classList.add("d-none");
        let linhas = "";
        for (const permissao of resposta) {
            linhas += `<li class="list-group-item shadow mb-2 flex-column align-items-start">
            <div class="d-flex w-100 justify-content-start">
                <h6 class="mb-1 font-weight-bold" style="font-size: 14pt;">
                    ${permissao.nome}
                </h6>
            </div>
            <h5 class="mt-1 font-weight-bold" style="text-align:right;">
               ${permissao.codigo}
            </h5>
            <hr>
            <div class="d-flex w-100 justify-content-end">
                <button class="btn btn-grid-pg m-2" onclick="location.href='permissao.html?codigo=${permissao.codigo}'">
                    <span id="" class="spinner-grow spinner-grow-sm d-none"
                        role="status" aria-hidden="true"></span>
                    <i class="fas fa-edit"></i>
                </button>
            </div>
        </li>`;
        }
        lista.innerHTML = linhas;
    }
};