const permissaoNome = document.querySelector("#permissaoNome");
const permissaoRepresentante = document.querySelector("#permissaoRepresentante");
const listaPermissao = document.querySelector("#lista");
const formulario = document.querySelector("#formulario");
const buttonMarcaTodos = document.querySelector("#marTodos");

const permissaoControle = {
    permissao: {},
    aplicacoes: [],
    codigoPermissao: 0,
    alterar: false,
    async salvarPermissao() {
        let resposta;
        try {
            resposta = await (await permissaoServico.salvar(this.permissao)).json();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return Promise.reject(resposta);
            }
            this.mostrarMensagemSalvoComSucesso();
        } catch (error) {
            this.mostrarMensagemErroComunicacaoServidor();
            resposta = error;
        }
        return resposta;
    },
    async alterarPermissao() {
        let resposta;
        try {
            resposta = await (await permissaoServico.alterar(this.permissao, this.codigoPermissao)).text();
            if (resposta.mensagemDesenvolvedor) {
                swal(resposta.titulo, resposta.mensagemDesenvolvedor, "warning");
                return Promise.reject(resposta);
            }
            this.mostrarMensagemSalvoComSucesso();
        } catch (error) {
            this.mostrarMensagemErroComunicacaoServidor();
            resposta = error;
        }
        return resposta;
    },
    async buscarPermissaoPorCodigo() {
        const resposta = await (await permissaoServico.buscarPermissaoPor(this.codigoPermissao)).json();
        permissaoNome.value = resposta.nome;
        permissaoRepresentante.checked = resposta.representante;
    },
    async listarAplicacoes() {
        listaPermissao.innerHTML = "";
        let resposta;
        if (this.alterar) {
            resposta = await (await permissaoServico.listarPermissaoAplicacoes(this.codigoPermissao)).json();
        } else {
            resposta = await (await permissaoServico.listarAplicacoes()).json();
        }
        this.aplicacoes = resposta;
        let linhas = "";
        let index = 0;
        for (const aplicacao of this.aplicacoes) {
            aplicacao.selecao = aplicacao.status ? aplicacao.status : false;
            const selecionarAplicacaoAtiva = aplicacao.selecao ? "checked" : "";
            linhas += ` <li class="list-group-item">
            <div class="d-flex w-100 justify-content-start">
                <div class="checkbox">
                    <label style="font-size: 1em">
                        <input type="checkbox" class="selecao" value="" ${selecionarAplicacaoAtiva} onclick="permissaoControle.selecionar(${index},this)">
                        <span class="cr">
                            <i class="cr-icon fa fa-check"></i>
                        </span>
                    </label>
                </div>
                <h5 class="mb-1"> ${aplicacao.nome}</h5>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <label for="aplicacaoAcessar-${index}">Acessar</label>
                    <select id="aplicacaoAcessar-${index}" class="form-control" onchange="permissaoControle.mudarVisibilidade(${index},this,'acessar')">
                        <option ${aplicacao.acessar ? "selected" : ""} value="1">Sim</option>
                        <option ${aplicacao.acessar ? "" : "selected"} value="0">Não</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="aplicacaoIncluir-${index}">Incluir</label>
                    <select id="aplicacaoIncluir-${index}" class="form-control" onchange="permissaoControle.mudarVisibilidade(${index},this,'incluir')">
                        <option ${aplicacao.acessar ? "selected" : ""} value="1">Sim</option>
                        <option ${aplicacao.acessar ? "" : "selected"} value="0">Não</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="aplicacaoAlterar-${index}">Alterar</label>
                    <select id="aplicacaoAlterar-${index}" class="form-control" onchange="permissaoControle.mudarVisibilidade(${index},this,'alterar')">
                        <option ${aplicacao.acessar ? "selected" : ""} value="1">Sim</option>
                        <option ${aplicacao.acessar ? "" : "selected"} value="0">Não</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="aplicacaoExcluir-${index}">Excluir</label>
                    <select id="aplicacaoExcluir-${index}" class="form-control" onchange="permissaoControle.mudarVisibilidade(${index},this,'excluir')">
                        <option ${aplicacao.acessar ? "selected" : ""} value="1">Sim</option>
                        <option ${aplicacao.acessar ? "" : "selected"} value="0">Não</option>
                    </select>
                </div>
            </div>
        </li>`;
            index++;
        }
        listaPermissao.innerHTML = linhas;
    },
    async salvarPermissaoAplicacao(codigoPermissao) {
        const permissaoAplicacoes = this.aplicacoes.map(aplicacao => {
            const aplicacaoSalva = {
                codigoPermissao: codigoPermissao,
                codigoAplicacao: aplicacao.codigo,
                acessar: !aplicacao.acessar ? false : aplicacao.acessar,
                alterar: !aplicacao.alterar ? false : aplicacao.alterar,
                excluir: !aplicacao.excluir ? false : aplicacao.excluir,
                incluir: !aplicacao.incluir ? false : aplicacao.incluir,
                status: aplicacao.selecao,
            };
            return aplicacaoSalva;
        });
        permissaoServico.salvarPermissaoAplicacao(permissaoAplicacoes);
    },
    selecionarTodos() {
        const selecoes = document.querySelectorAll(".selecao");
        if (buttonMarcaTodos.checked) {
            for (let i = 0; i < selecoes.length; i++) {
                const selecao = selecoes[i];
                selecao.checked = true;
                this.aplicacoes[i].selecao = true;
            }
            return;
        }
        for (let i = 0; i < selecoes.length; i++) {
            const selecao = selecoes[i];
            selecao.checked = false;
            this.aplicacoes[i].selecao = false;
        }
    },
    selecionar(index, input) {
        input.checked = !!input.checked;
        this.aplicacoes[index].selecao = input.checked;
    },
    mudarVisibilidade(index, select, tipoAcesso) {
        const valorSelecionado = select.value;
        this.aplicacoes[index][tipoAcesso] = new Boolean(parseInt(valorSelecionado)).valueOf();
    },
    mostrarInformacoes() {
        const parametro = window.location.search.slice(1);
        const permissaoID = parametro.replace(/codigo=/, "");
        if (!permissaoID) {
            this.alterar = false;
            this.listarAplicacoes();
            return;
        }
        this.alterar = true;
        this.codigoPermissao = permissaoID;
        this.listarAplicacoes();
        this.buscarPermissaoPorCodigo();
    },
    mostrarMensagemSalvoComSucesso() {
        swal("Permissão salvo com sucesso", "Morpheus", "success");
    },
    mostrarMensagemErroComunicacaoServidor() {
        swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
    }
}
formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    delete permissaoControle.permissao.codigo;
    permissaoControle.permissao.nome = permissaoNome.value;
    permissaoControle.permissao.representante = permissaoRepresentante.checked;
    if (permissaoControle.alterar) {
        permissaoControle.alterarPermissao().then(() => permissaoControle.salvarPermissaoAplicacao(permissaoControle.codigoPermissao));
    } else {
        permissaoControle.salvarPermissao().then(resposta => permissaoControle.salvarPermissaoAplicacao(resposta.codigo));
    }
});

buttonMarcaTodos.addEventListener("click", evt => {
    permissaoControle.selecionarTodos();
});