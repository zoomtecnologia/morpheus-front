const formulario = document.querySelector("#formulario");
const arquivoCobranca = document.querySelector("#arquivoCobranca");
const buttonEnviarArquivo = document.querySelector("#buttonEnviarArquivo");
const loadingEnviarArquivo = document.querySelector("#loadingEnviarArquivo");
const lista = document.querySelector("#lista");

const importacaoServico = {
    async enviarArquivo(formData) {
        return fetch(`${servidor.URL_API}/validacao-cobranca/upload-cobrancas`, {
            method: "POST",
            body: formData,
            headers: {
                'Authorization': usuario.autorizacao
            }
        });
    }
};

const importacaoControle = {
    formData: new FormData(),
    async enviarArquivo() {
        try {
            const resposta = await (await importacaoServico.enviarArquivo(this.formData)).json();
            buttonEnviarArquivo.disabled = false;
            loadingEnviarArquivo.classList.add("d-none");
            swal("Arquivo importado com sucesso!!", "Morpheus", "success");
            this.listarCobrancasProcessadas(resposta);
        } catch (error) {
            buttonEnviarArquivo.disabled = false;
            loadingEnviarArquivo.classList.add("d-none");
            swal("Não foi possivel realizar o login", "sistema indisponível! tente novamente mais tarde.", "error");
        }
    },
    listarCobrancasProcessadas(resposta) {
        lista.innerHTML = "";
        let linhas = "";
        for (const cobranca of resposta) {
            let badgeStatusPagamento = "badge-success";
            if (cobranca.statusPagamento == "Inadimplente") {
                badgeStatusPagamento = "badge-warning";
            } else if (cobranca.statusPagamento == "Cancelado") {
                badgeStatusPagamento = "badge-danger";
            }
            linhas += `
            <li class="list-group-item">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1 font-weight-bold">${cobranca.razaoSocial}</h5>
                    <small>${cobranca.cnpj}</small>
                </div>
                <p class="mb-1">${cobranca.mes}</p>
                <span class="badge ${badgeStatusPagamento}">${cobranca.statusPagamento}</span>
            </li>`;
        }
        lista.innerHTML = linhas;
    }
};

arquivoCobranca.addEventListener("change", evt => {
    importacaoControle.formData.append("cobrancas", evt.target.files[0]);
});

formulario.addEventListener("submit", evt => {
    evt.preventDefault();
    buttonEnviarArquivo.disabled = true;
    loadingEnviarArquivo.classList.remove("d-none");
    importacaoControle.enviarArquivo();
});