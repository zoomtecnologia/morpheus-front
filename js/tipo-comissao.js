const tipoComissaoServico = {
    urlTipoComissao: "/tipo-comissoes",
    async salvarTipoComissao(tipoComissao) {
        return requisicao.requisitarMetodoPOST(this.urlTipoComissao, tipoComissao, false);
    },
    async alterarTipoComissao(tipoComissao, codigoTipoComissao) {
        delete tipoComissao.codigo;
        return requisicao.requisitarMetodoPUT(`${this.urlTipoComissao}/${codigoTipoComissao}`, tipoComissao, false, false);
    },
    async buscarTipoComissao(codigoTipoComissao) {
        return requisicao.requisitarMetodoGET(`${this.urlTipoComissao}/${codigoTipoComissao}`);
    }
};

const tipoComissaoControle = {
    tipoComisao: {},
    tipoComissaoAlteracao: false,
    codigoTipoComissao: 0,
    async salvarTipoComissao() {
        this.preencherTipoComissaoParaSalvar();
        let resposta = {};
        if (this.tipoComissaoAlteracao) {
            resposta = await (await tipoComissaoServico.alterarTipoComissao(this.tipoComisao, this.codigoTipoComissao)).text();
        } else {
            resposta = await (await tipoComissaoServico.salvarTipoComissao(this.tipoComisao)).json();
        }
        if (resposta.mensagemDesenvolvedor) {
            swal("AVISO", resposta.titulo, "warning");
            return;
        }
        if (!this.tipoComissaoAlteracao) {
            this.limparCampos();
        }
        swal("Tipo de comissão Salva", "", "success");
    },
    async buscarTipoComissao() {
        const resposta = await (await tipoComissaoServico.buscarTipoComissao(this.codigoTipoComissao)).json();
        this.tipoComisao = resposta;
        this.preencherTipoComissaoParaTela();
    },
    preencherTipoComissaoParaSalvar() {
        const nome = document.getElementById("nome").value;
        const porcentagemRepresentante = document.getElementById("porcentagemRepresentante").value;
        const porcentagemZoom = document.getElementById("porcentagemZoom").value;
        const inativo = document.getElementById("inativo");
        const valorMinimo = document.getElementById("valorMinimo").value;
        const valorSemComissao = document.getElementById("valorSemComissao").value;
        const tipo = document.getElementById("tipoComissao").value;
        this.tipoComisao.nome = nome;
        this.tipoComisao.porcentagemRepresentante = porcentagemRepresentante;
        this.tipoComisao.porcentagemZoom = porcentagemZoom;
        this.tipoComisao.status = inativo.checked ? "I" : "A";
        this.tipoComisao.valorMinimo = valorMinimo;
        this.tipoComisao.valorSemComissao = valorSemComissao;
        this.tipoComisao.tipo = tipo;
    },
    preencherTipoComissaoParaTela() {
        document.getElementById("nome").value = this.tipoComisao.nome;
        document.getElementById("porcentagemRepresentante").value = this.tipoComisao.porcentagemRepresentante;
        document.getElementById("porcentagemZoom").value = this.tipoComisao.porcentagemZoom;
        document.getElementById("inativo").checked = this.tipoComisao.status == "I";
        document.getElementById("valorMinimo").value = this.tipoComisao.valorMinimo;
        document.getElementById("valorSemComissao").value = this.tipoComisao.valorSemComissao;
        document.getElementById("tipoComissao").value = this.tipoComisao.tipo;
    },
    limparCampos() {
        document.getElementById("nome").value = "";
        document.getElementById("porcentagemRepresentante").value = "";
        document.getElementById("porcentagemZoom").value = "";
        document.getElementById("inativo").checked = false;
        document.getElementById("valorMinimo").value = "";
        document.getElementById("valorSemComissao").value = "";
        document.getElementById("tipoComissao").value = "";
        this.tipoComisao = null;
        this.tipoComisao = {};
        this.codigoTipoComissao = 0;
        this.tipoComissaoAlteracao = false;
    },
    adicionarEventos() {
        const formulario = document.getElementById("formulario");
        formulario.addEventListener("submit", evt => {
            evt.preventDefault();
            this.salvarTipoComissao();
        });
    },
    mostrarInformacoesNaTela() {
        const parametro = window.location.search.slice(1);
        const codigoTipoComissao = parametro.replace(/tipo-comissao=/, "");
        if (!codigoTipoComissao) {
            this.tipoComissaoAlteracao = false;
            return;
        }
        this.tipoComissaoAlteracao = true;
        this.codigoTipoComissao = codigoTipoComissao;
        this.buscarTipoComissao(codigoTipoComissao);
    }

};