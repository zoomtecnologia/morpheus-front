const usuario = JSON.parse(localStorage.getItem("usuario"));
if (!usuario) {
    window.location.href = "index.html";
}
const pathDaUrl = `http://${window.location.host}/onsaiph/onsaiph/controle/`;
const numberformat = new Intl.NumberFormat('de-DE');
const chartPie = {
    dadosDoGrafico: function (dados) {
        return {
            labels: ["Dinheiro", "TEF", "POS", "Vales", "Ticket"],
            datasets: [{
                data: [dados.dinheiro, dados.tef, dados.cartaoPOS, dados.vales, dados.ticket],
                backgroundColor: ['#1cc88a', '#4e73df', '#36b9cc', '#e74a3b', '#f6c23e'],
                hoverBackgroundColor: ['#1cc88a', '#4e73df', '#36b9cc', '#e74a3b', '#f6c23e'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        };
    },
    options: function () {
        return {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: true,
                caretPadding: 10,
                callbacks: {
                    label: function (tooltipItem, chart) {
                        let label = chart.labels[tooltipItem.index];
                        let valor = chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                        return `${label} R$ ${numberformat.format(valor)}`;
                    }
                }
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        };
    },
    criarGrafico: function (dados) {
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';
        let ctx = document.getElementById("myPieChart");
        let semMovimentacaoChart = document.getElementById("chartPieSemMovimentacao");
        if (dados.dinheiro != "0.00" || dados.tef != "0.00" || dados.cartaoPOS != "0.00" || dados.vales != "0.00" || dados.ticket != "0.00") {
            ctx.style.display = "block";
            semMovimentacaoChart.style.display = "none";
        } else {
            semMovimentacaoChart.style.display = "block";
            ctx.style.display = "none";
        }
        window.myPieChart = new Chart(ctx, {
            type: 'doughnut',
            data: this.dadosDoGrafico(dados),
            options: this.options(),
        });
    }
};

const formataData = {
    formtador: function (data) {
        let ano = new Intl.DateTimeFormat('pt-Br', { year: 'numeric' }).format(data);
        let mes = new Intl.DateTimeFormat('pt-Br', { month: '2-digit' }).format(data);
        let dia = new Intl.DateTimeFormat('pt-Br', { day: '2-digit' }).format(data);
        return `${ano}-${mes}-${dia}`;
    },
    pegarPeridoDoMes: function () {
        let date = new Date();
        let primeiroDia = new Date(date.getFullYear(), date.getMonth(), 1);
        let ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        const dataInicial = primeiroDia.toISOString().slice(0, 10) + " 00:00:00";
        const dataFinal = ultimoDia.toISOString().slice(0, 10) + " 23:59:59";
        return { dataInicial: dataInicial, dataFinal: dataFinal };
    },
    pegarPeriodoDoDia: function () {
        const dataInicial = this.formtador(new Date()) + " 00:00:00";
        const dataFinal = this.formtador(new Date()) + " 23:59:59";
        return { dataInicial: dataInicial, dataFinal: dataFinal };
    },
    pegarPeriodoDaSemana: function () {
        const dataInicial = this.formtador(new Date()) + " 00:00:00";
        let dataAtual = new Date();
        dataAtual.setDate(dataAtual.getDate() - 7);
        const dataFinal = this.formtador(dataAtual) + " 23:59:59";
        return { dataInicial: dataFinal, dataFinal: dataInicial };
    }
}

const empresaService = {
    listarEmpresa: async function listarEmpresas() {
        const url = `${pathDaUrl}EmpresaControle.php?cnpj=${usuario.cnpj}`;
        const promisse = await fetch(url);
        return promisse;
    }
};

const dashboardService = {
    listarTotais: async function listarTotais(empresa, dataAtual) {
        const url = `${pathDaUrl}DashboardControle.php`;
        const dataIncial = dataAtual.dataInicial;
        const dataFinal = dataAtual.dataFinal;
        const status = "N";
        let parametros = `?cnpj=${usuario.cnpj}&datainicial=${dataIncial}&datafinal=${dataFinal}`;
        parametros += `&status=${status}&empresa=${empresa}`;
        const promisse = await fetch(`${url}${parametros}`);
        return promisse;
    },
    listarFaturamentoAnual: async function () {
        const url = `${pathDaUrl}DashboardFaturamentoAnualControle.php?cnpj=${usuario.cnpj}`;
        const promisse = await fetch(url);
        return promisse;
    },
    listarItensMaisVendidos: async function (empresa, dataAtual) {
        const dataInicial = dataAtual.dataInicial;
        const dataFinal = dataAtual.dataFinal;
        let url = `${pathDaUrl}ItensVendidoControle.php`;
        url += `?cnpj=${usuario.cnpj}&datainicial=${dataInicial}&datafinal=${dataFinal}&empresa=${empresa}`;
        const promisse = await fetch(url);
        return promisse;
    },
    listarTotalDasVendas: async function (empresa, dataAtual) {
        const dataInicial = dataAtual.dataInicial;
        const dataFinal = dataAtual.dataFinal;
        let url = `${pathDaUrl}TotaisHistoricoControle.php`;
        url += `?cnpj=${usuario.cnpj}&datainicial=${dataInicial}&datafinal=${dataFinal}&empresa=${empresa}`;
        const promisse = await fetch(url);
        return promisse;
    },
    listarFaturamentoPorDia: async function (empresa, dataAtual) {
        const dataInicial = dataAtual.dataInicial;
        const dataFinal = dataAtual.dataFinal;
        let url = `${pathDaUrl}DashboardFaturamentoPorDiaControle.php`;
        url += `?cnpj=${usuario.cnpj}&datainicial=${dataInicial}&datafinal=${dataFinal}&empresa=${empresa}`;
        const promisse = await fetch(url);
        return promisse;
    }
};

const dashboardControle = {
    configuracaoGraficoDeBarra: {
        titulos: [""],
        valores: [0],
        tituloValores: "Vendas",
        coresDeFundo: "#4e73df",
        coresHover: "#4e73df",
        coresDaBorda: "#4e73df"
    },
    pegarEmpresaSelecionada: function () {
        const selecaoEmpresa = document.getElementById("empresas");
        const empresa = selecaoEmpresa.options[selecaoEmpresa.selectedIndex].value;
        return empresa;
    },
    preencherComboBox: async function preencherComboBox() {
        const comboEmpresas = document.getElementById("empresas");
        const empresas = await (await empresaService.listarEmpresa()).json();
        const option = `<option selected value="">Todas as empresas</option>`;
        comboEmpresas.innerHTML += option;
        for (let empresa of empresas) {
            const option = `<option value="${empresa.codigo}">${empresa.codigo} - ${empresa.razaoSocial}</option>`;
            comboEmpresas.innerHTML += option;
        }
    },
    preencherValoresDashBoard: function preencherValoresDashBoard(totais) {
        const totalVendas = document.getElementById("totalVendas");
        const ticketMedio = document.getElementById("ticketMedio");
        const quantidadeVendas = document.getElementById("quantidadeVendas");
        const itensVendidos = document.getElementById("itensVendidos");
        totalVendas.textContent = totais.totalVendas;
        ticketMedio.textContent = totais.ticketMedio;
        quantidadeVendas.textContent = totais.quantidadeVendas;
        itensVendidos.textContent = totais.quantidadeItens;
        chartPie.criarGrafico(totais);
        const nomeDoMes = document.getElementById("nomeDoMes");
        if (nomeDoMes.textContent.trim() === "Atual") {
            nomeDoMes.setAttribute("data-content", totais.totalVendas);
        }
    },
    mostrarTotais: function mostrarTotais() {
        const empresa = this.pegarEmpresaSelecionada();
        const dataAtual = formataData.pegarPeridoDoMes();
        this.listarInformacoesDashboard(empresa, dataAtual).then(totais => this.preencherValoresDashBoard(totais));
        this.listarItensVendidosDashBoard(dataAtual);
        this.listarHistoricoVendas(dataAtual);
        this.listarFaturamentoPorDia(dataAtual).then(resposta => this.criarGraficoDeBarra(resposta.conteudos));
    },
    listarInformacoesDashboard: async function listarInformacoesDashboard(empresa, dataAtual) {
        const totais = await (await dashboardService.listarTotais(empresa, dataAtual)).json();
        return totais;
    },
    listarVendasDia: function () {
        const empresa = this.pegarEmpresaSelecionada();
        const dataAtual = formataData.pegarPeriodoDoDia();
        window.myPieChart.destroy();
        this.listarInformacoesDashboard(empresa, dataAtual).then(totais => this.preencherValoresDashBoard(totais));
        this.listarItensVendidosDashBoard(dataAtual);
        this.listarHistoricoVendas(dataAtual);
        const tituloPeriodo = document.getElementById("tituloPeriodo");
        tituloPeriodo.textContent = "Hoje";
    },
    listarInformacoesDashBoardPorMes: function () {
        const empresa = this.pegarEmpresaSelecionada();
        const dataAtual = formataData.pegarPeridoDoMes();
        window.myPieChart.destroy();
        this.listarInformacoesDashboard(empresa, dataAtual).then(totais => this.preencherValoresDashBoard(totais));
        this.listarItensVendidosDashBoard(dataAtual);
        this.listarHistoricoVendas(dataAtual);
        const tituloPeriodo = document.getElementById("tituloPeriodo");
        tituloPeriodo.textContent = "Mês";
    },
    listarInformacoesDashBoardPorSemana: function () {
        const empresa = this.pegarEmpresaSelecionada();
        const dataAtual = formataData.pegarPeriodoDaSemana();
        window.myPieChart.destroy();
        this.listarInformacoesDashboard(empresa, dataAtual).then(totais => this.preencherValoresDashBoard(totais));
        this.listarItensVendidosDashBoard(dataAtual);
        this.listarHistoricoVendas(dataAtual);
        const tituloPeriodo = document.getElementById("tituloPeriodo");
        tituloPeriodo.textContent = "Semana";
    },
    listarInformacoesDashboardFaturamentoAnual: async function () {
        const faturamentoAnual = await (await dashboardService.listarFaturamentoAnual()).json();
        return faturamentoAnual;
    },
    listarItensVendidosDashBoard: async function (dataAtual) {
        const empresa = this.pegarEmpresaSelecionada();
        const resposta = await (await dashboardService.listarItensMaisVendidos(empresa, dataAtual)).json();
        const corpoDaTabelaItensVendido = document.getElementById('linhasItensVendidos');
        let linhas = "";
        corpoDaTabelaItensVendido.innerHTML = "";
        for (let item of resposta) {
            linhas += `
                <tr>
                    <td>${item.codigoProduto}</td>    
                    <td>${item.descricao}</td>
                    <td>${item.quantidade}</td>
                    <td>${item.valorTotal}</td>
                </tr>
            `;
        }
        corpoDaTabelaItensVendido.innerHTML += linhas;
    },
    listarHistoricoVendas: async function (dataAtual) {
        const empresa = this.pegarEmpresaSelecionada();
        const resposta = await (await dashboardService.listarTotalDasVendas(empresa, dataAtual)).json();
        const vendasAutorizadas = document.getElementById("vendasAutorizadas");
        const vendasContingencia = document.getElementById("vendasContingencia");
        const vendasCanceladas = document.getElementById("vendasCanceladas");
        const totalVendas = document.getElementById("totalVendasHistorico");
        vendasAutorizadas.textContent = resposta.totalAutorizados;
        vendasContingencia.textContent = resposta.totalContingencia;
        vendasCanceladas.textContent = resposta.totalCancelados;
        totalVendas.textContent = resposta.totalVendas;
    },
    logout: function () {
        localStorage.removeItem("filtroCaixa");
        localStorage.removeItem("usuario");
        window.location.href = "index.html";
    },
    criarGraficoDeBarra: function (dados) {
        const date = new Date();
        const diaPrimeiro = new Date(date.getFullYear(), date.getMonth(), 1).getDate();
        const ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
        const consultaPorDia = dados;
        const diasDoMes = [];
        const valoresPorDia = [];
        for (let i = diaPrimeiro; i <= ultimoDia; i++) {
            const dia = ("00" + i).slice(-2);
            diasDoMes.push(dia);
            let totalDoDia = 0;
            if (consultaPorDia.some(elemento => {
                if (elemento.diaDaVenda == dia) {
                    totalDoDia = elemento.totalDeVendas;
                    return true;
                }
                return false;
            })) {
                valoresPorDia[i - 1] = totalDoDia;
            } else {
                valoresPorDia[i - 1] = totalDoDia;
            }
        }
        this.configuracaoGraficoDeBarra.titulos = diasDoMes;
        this.configuracaoGraficoDeBarra.valores = valoresPorDia;
        criarGraficoDeBarras(dashboardControle.configuracaoGraficoDeBarra);
    },
    listarFaturamentoPorDia: async function (dataAtual) {
        const empresaSelecionada = this.pegarEmpresaSelecionada();
        const resposta = await (await dashboardService.listarFaturamentoPorDia(empresaSelecionada, dataAtual)).json();
        return resposta;
    },
    listarFaturamentoPorDiaAtravesDoMes: async function (mes) {
        const dataDeHoje = new Date();
        dataDeHoje.setMonth(mes);
        const dataDoMesSelecionado = dataDeHoje;
        dataDoMesSelecionado.setDate(1);
        const dataInicioDoMes = dataDoMesSelecionado.toISOString().slice(0, 10) + " 00:00:00";
        const dataFinalDoMes = new Date(dataDoMesSelecionado.getFullYear(), dataDoMesSelecionado.getMonth() + 1, 0).toISOString().slice(0, 10) + " 00:00:00";
        const resposta = await this.listarFaturamentoPorDia({ dataInicial: dataInicioDoMes, dataFinal: dataFinalDoMes });
        window.myBarChart.destroy();
        this.criarGraficoDeBarra(resposta.conteudos);
        return resposta;
    }
}

const linksDoMes = document.querySelectorAll(".meses");
linksDoMes.forEach(link => {
    link.onclick = function (event) {
        dashboardControle.listarFaturamentoPorDiaAtravesDoMes(link.getAttribute("data-value"))
            .then(resposta => {
                const nomeDoMes = document.getElementById("nomeDoMes");
                nomeDoMes.textContent = link.textContent;
                nomeDoMes.setAttribute("data-content", "R$ " + resposta.totalRegistro);
            });
    };
});