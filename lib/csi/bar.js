/*
	Use this to make sure nobody tries to load this without CSI running
*/
if(!ns || !ns.tw || !ns.tw.csi) alert("CSI - Client Side Include has not been loaded.");

/*
	Always include other files BEFORE registering your current include,
	otherwise the last include will not be processed and the init code you
	added with registerInclude will never be processed.
	
	Make sure your paths are consistent. CSI uses only this string to determine
	if a file has already been added, so using "foo.js", "./foo.js" or any number
	of different notations will produce undesirable results.
*/
ns.tw.csi.include("csi/foo.js");

/*
	You can place code here, but remember that any libraries are not loaded yet, so only the stuff that's directly on the main
	page is available. that and ns.tw.nsa.create , a utility to create namespaces
*/
ns.tw.nsa.create("ns.tw.bar");

ns.tw.bar.class_messager=function(msg){
	this.message=msg;
}

	
/*
	Always include this command, even if the init function is empty (It also notifies CSI that the file has loaded).
	The function gets called when all scripts have finished loading. Make sure this is at the END of your JS file.
*/
ns.tw.csi.registerInclude(function(){
	ns.tw.bar.class_messager.prototype.show=function(){
		ns.tw.foo.modal(this.message);
	}
});